#pragma once
#include "D3DUtil.h"


class AudioManager
{
public:
	AudioManager();
	~AudioManager();


	void Init(ID3D11Device* device);

	Sound* CreateSound(std::string keyName, std::string filename);

	Sound* GetSound(std::string keyname);

	void ResetAllSounds();

private:
	AudioManager(const AudioManager& rhs);
	AudioManager& operator=(const AudioManager& rhs);

	std::map<std::string, Sound*> mAudioMap;

};

