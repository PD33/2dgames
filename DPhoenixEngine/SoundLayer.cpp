#include "Soundlayer.h"


SoundLayer* SoundLayer::mGlobalSLayer = NULL;

SoundLayer::SoundLayer(HWND hWnd)
{
	//Null pointers to directsound and primary mixer;
	mDSound = NULL;
	mPrimary = NULL;


	//if this is set it is already initialised
	if (mGlobalSLayer) {

		debug << "SoundLayer already initialized! \n";
	}

	mGlobalSLayer = this;

	HRESULT				hr;
	LPDIRECTSOUNDBUFFER pDSBPrimary = NULL;

	//Create the IDirect sound using the primk
	hr = DirectSoundCreate8(NULL, &mDSound, NULL);

	if (FAILED(hr)) {
		debug << "DirectSoundCreate failed!\n";
	}

	//Set the coop level to DSSCL_PRIORITY
	//Gives us more control over buffers and formats
	hr = mDSound->SetCooperativeLevel(hWnd, DSSCL_PRIORITY);
	if (FAILED(hr)) {
		debug << "SetCooperativeLevel (DS) failed!\n";
	}

	//Get the priamry buffer
	DSBUFFERDESC dsbd;
	dsbd.dwBufferBytes = 0;
	dsbd.lpwfxFormat = NULL;
	dsbd.dwSize = sizeof(DSBUFFERDESC);
	dsbd.dwFlags = DSBCAPS_PRIMARYBUFFER;
	dsbd.dwReserved = 0;
	dsbd.guid3DAlgorithm = GUID_NULL;
	
	//cREA THE PRIMARY SOUND BUFFER BASED ON DESe
	hr = mDSound->CreateSoundBuffer(&dsbd, &pDSBPrimary, NULL);

	if (FAILED(hr)) {
		debug << "CreateSoundBuffer (DS) failed!\n";
	}

	WAVEFORMATEX wfx;
	ZeroMemory(&wfx, sizeof(WAVEFORMATEX));

	wfx.wFormatTag = WAVE_FORMAT_PCM;
	wfx.nChannels = 2;
	wfx.nSamplesPerSec = 22050;
	wfx.wBitsPerSample = 16;
	wfx.nBlockAlign = wfx.wBitsPerSample / 8 * wfx.nChannels;
	wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;

	//Set primary buffer format
	if (FAILED(hr = pDSBPrimary->SetFormat(&wfx))) {
		debug << "SetFormat (DS) failed! \n";
	}


	pDSBPrimary->Release();

}

SoundLayer::~SoundLayer() {

	mPrimary->Release();
	mDSound->Release();
	mGlobalSLayer = NULL;
}