#pragma once

class IEvent //This is a interface class so it uses the I prefex before the name
{
public:
	IEvent();
	virtual ~IEvent();

	int GetID() { return mId; }

	enum EventTypes {
		EVENT_KEYPRESS,
		EVENT_KEYRELEASE,
		EVENT_MOUSECLICK,
		EVENT_MOUSEMOTION,
		EVENT_MOUSEWHEEL,
		EVENT_MOUSEMOVE,
		EVENT_XBOX_INPUT
	};


protected:
	int mId; //Unqiue id tag



};


class KeyPressEvent : public IEvent
{
public:
	int mKeycode;				//Direct Input code
	KeyPressEvent(int key);		//Raise the event in code;

};

class KeyReleaseEvent : public IEvent
{
public:
	int mKeycode;				//Direct Input code
	KeyReleaseEvent(int key);		//Raise the event in code;

};

class MouseClickEvent : public IEvent
{
public:
	int mButton;								//Direct Input code
	int mPosX; int mPosY;						//Position
	MouseClickEvent(int btn, int x, int y);		//Raise the event in code;

};

class MouseMotionEvent : public IEvent
{
public:										
	int mDeltaX; int mDeltaY;				//DeltaPositon
	MouseMotionEvent(int dx, int dy);		//Raise the event in code;

};

class MouseMoveEvent : public IEvent
{
public:
	int mPosX; int mPosY;				//Positon
	MouseMoveEvent(int x, int y);		//Raise the event in code;

};

class MouseWheelEvent : public IEvent
{
public:								//Direct Input code
	int mWheel;						//Scrool amount
	MouseWheelEvent(int wheel);		//Raise the event in code;

};

class XboxEvent : public IEvent
{
public:								//Direct Input code
	XINPUT_STATE mPadState;
	XBoxPad* mPadPointer;
	XboxEvent(XINPUT_STATE padState, XBoxPad* padPtr);	//Raise the event in code;

};








