#pragma once
#include "D3DUtil.h"


class TextureManager
{
public:
	TextureManager();
	~TextureManager();



	void Init(ID3D11Device* device);

	//Uses stl map - Create texture new if not already stored, or presented stored texture
	ID3D11ShaderResourceView* CreateTexture(std::string filename);

private:

	//private contrstructor
	TextureManager(const TextureManager& rhs);
	TextureManager& operator=(const TextureManager& rhs);

	//Pointer to d3d device
	ID3D11Device* md3dDevice;
	//this helps us manage loaded textures
	std::map<std::string, ID3D11ShaderResourceView*> mTextureSRV;


};

