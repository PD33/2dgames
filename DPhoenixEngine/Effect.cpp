#include "Effect.h"

#pragma region Effect
Effect::Effect(ID3D11Device * device, const std::string & fileName) : mfx(0)
{

	DWORD shaderFlags = 0;

#if defined(DEBUG) || defined(_DEBUG)
	shaderFlags |= D3D10_SHADER_DEBUG;
	shaderFlags != D3D10_SHADER_SKIP_OPTIMIZATION;
#endif

	//The blob (for data compulation
	ID3D10Blob* compiledShader = 0;
	ID3D10Blob* compiledMsgs = 0;

	//load this particular shader

	HRESULT hr = D3DX11CompileFromFile(fileName.c_str(), 0, 0, 0, "fx_5_0", shaderFlags, 0, 0, &compiledShader, &compiledMsgs, 0);

	if (compiledMsgs != 0) {
		MessageBoxA(0, (char*)compiledMsgs->GetBufferPointer(), 0, 0);
		ReleaseCOM(compiledMsgs);
	}


	if (FAILED(hr)) {


		DXTrace(__FILE__, (DWORD)__LINE__, hr, "D3DX11ComplieFromFile", true);
	}
	
	HR(D3DX11CreateEffectFromMemory(compiledShader->GetBufferPointer(), compiledShader->GetBufferSize(), 0, device, &mfx));


	ReleaseCOM(compiledShader);


}

Effect::~Effect() {

	ReleaseCOM(mfx);
}

#pragma endregion

#pragma region SpriteEffect

SpriteEffect::SpriteEffect(ID3D11Device* device, const std::string& filename) : Effect(device, filename) {

	//Set the spritetect to the shader techinque
	SpriteTect = mfx->GetTechniqueByName("Sprite");

	//link effects variabes to the shader variables
	WorldViewProj = mfx->GetVariableByName("gWorldViewProj")->AsMatrix();
	WorldViewProjTex = mfx->GetVariableByName("gWorldViewProjTex")->AsMatrix();
	World = mfx->GetVariableByName("gWorld")->AsMatrix();

	//asprite texture
	SpriteTex = mfx->GetVariableByName("gSpriteTex")->AsShaderResource();

	//float values
	Column = mfx->GetVariableByName("gColumn")->AsScalar();
	NumCols = mfx->GetVariableByName("gNumCols")->AsScalar();
	Width = mfx->GetVariableByName("gWidth")->AsScalar();
	FlipValue = mfx->GetVariableByName("gFlipValue")->AsScalar();
	LerpValue = mfx->GetVariableByName("gLerpValue")->AsScalar();
	OpacityValue = mfx->GetVariableByName("gOpacityValue")->AsScalar();
	//vector (colour) value
	AltColorValue = mfx->GetVariableByName("gAltColor")->AsVector();
}

SpriteEffect::~SpriteEffect()
{
}


#pragma endregion


#pragma region Effects

SpriteEffect*	Effects::SpriteFX = 0;

void Effects::InitAll(ID3D11Device* device) {

	SpriteFX = new SpriteEffect(device, "FX/Sprite.fx");
}


void Effects::DestoryAll() {

	SafeDelete(SpriteFX);
}





#pragma endregion 