#pragma once
#include "D3DUtil.h"



enum TileTypes {
	VOID_TILE,
	BLOCK_TILE,
	ONEWAY_TILE
};

struct BitMaskTile {
	TileTypes type;
	int bitMaskValue;
};

class Map
{
public:
	Map(std::string filname, TextureManager* mTexMgr, ID3D11Device* md3dDevice,int tileWidthPerPanel,int tileHeightPerPanel,int amtPanelsWide,int amtPanelsHigh);
	~Map();

	XMFLOAT2 GetMapTileAtPoint(XMFLOAT2 point);
	int GetMapTileYAtPoint(float y);
	int GetMapTileXAtPoint(float x);

	XMFLOAT2 GetMapTilePosition(int tileIndexX, int tileIndexY);
	XMFLOAT2 GetMapTilePosition(XMFLOAT2 tileCoords);

	BitMaskTile GetTile(int x, int y);
	void AssignBitmaskValues();
	bool OutOfRange(int x, int y, std::vector<std::vector<BitMaskTile>>& arr) {
		if (x < 0 || y < 0 || y > arr[0].size()-1|| x > arr.size()-1)
			return true;
		else
			return false;


	}

	bool IsObstacle(int x, int y);
	bool IsGround(int x, int y);
	bool IsOneWayPlatform(int x, int y);
	bool IsEmpty(int x, int y);

	std::vector<std::vector<BitMaskTile>> mTiles;

	Sprite* mOneWayTileSprite;
	Sprite* mSoildTileSprite;
	Sprite* mCollidedTileSprite;

	XMFLOAT3 mPosition;
	//Width and height
	int width;
	int height;

	XMFLOAT2 mPlayerOneSpawn;
	XMFLOAT2 mPlayerTwoSpawn;

	int mTileSize;
};

