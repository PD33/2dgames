﻿#pragma once


#include "D3DUtil.h"


std::map<WaveSoundRead*, int> Sound::mWaveMap;

void Sound::Init()
{


	DSBUFFERDESC dsbd;
	dsbd.dwFlags = DSBCAPS_CTRLVOLUME;
	dsbd.dwBufferBytes = mWaveSoundRead->mCkIn.cksize;
	dsbd.lpwfxFormat = mWaveSoundRead->mWfx;
	dsbd.guid3DAlgorithm = GUID_NULL;
	dsbd.dwSize = sizeof(DSBUFFERDESC);
	dsbd.dwReserved = 0;

	HRESULT hr;

	LPDIRECTSOUNDBUFFER pTempBuffer = 0;
	//Getting SoundPtr() was causing errors so I bypassed it my just calling GetSound()
	hr = SoundLayer::SoundPtr()->GetDSound()->CreateSoundBuffer(&dsbd, &pTempBuffer, NULL);
	if (FAILED(hr)) 
	{

		debug << "CreateSoundBuffer Failed! \n";
	}

	pTempBuffer->QueryInterface(IID_IDirectSoundBuffer8, (void**)&mBuffer);

	if (FAILED(hr)) 
	{
		debug << "SoundBuffer query to 8 failed \n";
	}

	pTempBuffer->Release();

	mBufferSize = dsbd.dwBufferBytes;

}

Sound::Sound(char * filename)
{

	mWaveSoundRead = NULL;
	mBuffer = NULL;

	//create a new wave file class
	mWaveSoundRead = new WaveSoundRead();
	mWaveMap[mWaveSoundRead] = 1;

	if (FAILED(mWaveSoundRead->Open(filename))) {
		debug << "Can not open sound file! " << filename << "\n";
	}


	Init();
	Fill();

}

Sound::Sound(Sound & in)
{

	mWaveSoundRead = in.mWaveSoundRead;

	mWaveMap[mWaveSoundRead]++;

	Init();
	Fill();

}

Sound & Sound::operator=(const Sound & in)
{
	// TODO: insert return statement here

	int count = --mWaveMap[mWaveSoundRead];

	if (!count) {
		delete mWaveSoundRead;
	}
	mBuffer->Release();

	mWaveSoundRead = in.mWaveSoundRead;
	mWaveMap[mWaveSoundRead]++;

	Init();
	Fill();

	return *this;

}

Sound::~Sound()
{
	int count = --mWaveMap[mWaveSoundRead];

	if (!count) {
		delete mWaveSoundRead;
	}
	else {
		mWaveMap[mWaveSoundRead] = count - 1;
	}

	mBuffer->Release();

}

void Sound::Restore()
{

	HRESULT hr;

	if (NULL == mBuffer) {

		return;
	}


	DWORD dwStatus;
	if (FAILED(hr = mBuffer->GetStatus(&dwStatus))) {
		debug << "SoundBuffer query to 8 failed \n";
	}

	if (dwStatus & DSBSTATUS_BUFFERLOST) {



		do {


			hr = mBuffer->Restore();
			if (hr == DSERR_BUFFERLOST) {
				Sleep(10);
			}
		} while (hr = mBuffer->Restore());




		Fill();
	}



}

void Sound::Fill()
{

	HRESULT hr;
	unsigned char* pbWavData;
	unsigned int cbWavSize;
	void* pbData	= NULL;
	void* pbData2	= NULL;
	unsigned long dwLength;
	unsigned long dwLength2;


	//How big is it? ( ͡° ͜ʖ ͡°)
	unsigned int nWaveFileSize = mWaveSoundRead->mCkIn.cksize;


	//Allocate enoguht space to hold the wave file data

	pbWavData = new unsigned char[nWaveFileSize];
	if (NULL == pbWavData) {

		delete[] pbWavData;
		debug << "Out of memory \n";
	}

	hr = mWaveSoundRead->Read(nWaveFileSize, pbWavData, &cbWavSize);

	if (FAILED(hr)) {

		delete[] pbWavData;
		debug << "Could not read \n";
	}

	mWaveSoundRead->Reset();

	hr = mBuffer->Lock(0, mBufferSize, &pbData, &dwLength, &pbData2, &dwLength2, 0);

	if (FAILED(hr)) {
		delete[] pbWavData;
		debug << "mBuffer->Lock failed \n";
	}

	memcpy(pbData, pbWavData, mBufferSize);
	mBuffer->Unlock(pbData, mBufferSize, NULL, 0);

	delete[] pbWavData;
}

void Sound::Play(bool bLoop)
{
	HRESULT hr;
	if (NULL == mBuffer)
		return;


	Restore();

	DWORD dwLooped = bLoop ? DSBPLAY_LOOPING : 0L;
	if (FAILED(hr = mBuffer->Play(0, 0, dwLooped))) {
		debug << "mBuffer->Play failed\n";
	}


}

bool Sound::IsPlaying()
{
	DWORD dwStatus = 0;
	mBuffer->GetStatus(&dwStatus);

	if(dwStatus & DSBSTATUS_PLAYING)
		return true;
	else
		return false;
}

void Sound::Stop()
{
	HRESULT hr;

	if (NULL == mBuffer)
		return;

	Restore();
	if (FAILED(hr = mBuffer->Stop())) {
		debug << "mBuffer->Stop failed \n";
	}


}

void Sound::SetPosition(int pos)
{
	mBuffer->SetCurrentPosition(pos);
}




