#include "AudioManager.h"



AudioManager::AudioManager()
{
}


AudioManager::~AudioManager()
{


	for (auto i = mAudioMap.begin(); i != mAudioMap.end(); ++i)
	{
		i->second->~Sound();
	}


	mAudioMap.clear();


}

void AudioManager::Init(ID3D11Device * device)
{



}

Sound * AudioManager::CreateSound(std::string keyName, std::string filename)
{
	Sound* sound = 0;

	if (mAudioMap.find(keyName) != mAudioMap.end()) {

		sound = mAudioMap[keyName];
	}
	else
	{
		char* fileNameConvert = new char[filename.size() + 1];
#pragma warning(suppress : 4996)
		strcpy(fileNameConvert, filename.c_str());

		//Load sound
		sound = new Sound(fileNameConvert);

		mAudioMap[keyName] = sound;

		delete[] fileNameConvert;
	}

	return sound;
}

Sound * AudioManager::GetSound(std::string keyname)
{
	if (mAudioMap.find(keyname) != mAudioMap.end()) 
	{
		return mAudioMap[keyname];
	}
	else 
	{

		return nullptr;
	}

}

void AudioManager::ResetAllSounds()
{


	for (std::map<std::string,Sound*>::iterator i = mAudioMap.begin(); i != mAudioMap.end(); ++i)
	{
		i->second->Stop();
		i->second->SetPosition(0);
	}

}
