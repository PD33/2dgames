#include "Entity2D.h"



Entity2D::Entity2D()
{
}


Entity2D::~Entity2D()
{
}

void Entity2D::UpdatePhysics(float dt, Map* map)
{
	//hold old values
	mOldPosition = mPosition;
	mOldSpeed = mSpeed;

	//Set old flags
	mOnGroundJust = mOnGroundIs;
	mAtCeilingJust = mAtCeilingIs;
	mPushRightWallJust = mPushRightWallIs;
	mPushLeftWallJust = mPushLeftWallIs;

	
	mPosition.x += mSpeed.x * dt;
	mPosition.y += mSpeed.y * dt;


	float leftWallX = 0;
	if (mSpeed.x < 0.0f && CollidesWithLeftWall(mOldPosition, mPosition, &leftWallX, map)) {
		mPosition.x = leftWallX + mAABB.halfSize.x - mAABBOffset.x;

		mPushLeftWallIs = true;
		mSpeed.x = min(mSpeed.x, 0.0f);

	}
	else
		mPushLeftWallIs = false;

	//right
	float rightWallX = 0;
	if (mSpeed.x > 0.0f && 
		CollidesWithRightWall(mOldPosition, mPosition, &rightWallX, map)) {

		mPosition.x = rightWallX - mAABB.halfSize.x - mAABBOffset.x;
		mPushRightWallIs = true;
		mSpeed.x = min(mSpeed.x, 0.0f);

	}
	else
		mPushRightWallIs = false;

	//Ground
	float groundY = 0;

	if (mSpeed.y <= 0.0f && HasGround(mOldPosition, mPosition, mSpeed, &groundY, map)) {

		mPosition.y = groundY + mAABB.halfSize.y - mAABBOffset.y;

		mSpeed.y = 0.0f;
		mOnGroundIs = true;

	}
	else
		mOnGroundIs = false;


	//Ceiling
	float ceilingY = 0;

	if (mSpeed.x >= 0.0f && HasCeiling(mOldPosition, mPosition, mSpeed, &ceilingY, map)) {


		mPosition.y = ceilingY - mAABB.halfSize.y - mAABBOffset.y - 1.0f;
		mSpeed.y = 0.0f;
		mAtCeilingIs = true;
	}
	else
		mAtCeilingIs = false;


	//Update collider collision bounds
	mAABB.center.x = mPosition.x + mAABBOffset.x;
	mAABB.center.y = mPosition.y + mAABBOffset.y;
}

bool Entity2D::HasGround(XMFLOAT2 oldPosition, XMFLOAT2 position, XMFLOAT2 speed, float * groundY, Map * map)
{
	XMFLOAT2 oldCenter(oldPosition.x + mAABBOffset.x, oldPosition.y + mAABBOffset.y);
	XMFLOAT2 center(position.x + mAABBOffset.x, position.y + mAABBOffset.y);


	XMFLOAT2 oldBottomLeft(oldCenter.x - mAABB.halfSize.x + 1.0f, oldCenter.y - mAABB.halfSize.y - 1.0f);
	XMFLOAT2 newBottomLeft(center.x - mAABB.halfSize.x + 1.0f, center.y - mAABB.halfSize.y - 1.0f);
	XMFLOAT2 newBottomRight(newBottomLeft.x + mAABB.halfSize.x * 2.0f - 2.0f, newBottomLeft.y);


	int endY = map->GetMapTileYAtPoint(newBottomLeft.y);
	int begY = max(map->GetMapTileYAtPoint(oldBottomLeft.y) - 1, endY);

	int dist = max(abs(endY - begY), 1);

	int tileIndexX;

	for (int tileIndexY = begY; tileIndexY >= endY; --tileIndexY) {


		XMVECTOR bottomLeftVec = XMVectorLerp(XMLoadFloat2(&newBottomLeft), XMLoadFloat2(&oldBottomLeft), (float)abs(endY - tileIndexY) / dist);
		XMFLOAT2 bottomLeft;
		XMStoreFloat2(&bottomLeft, bottomLeftVec);

		XMFLOAT2 bottomRight(bottomLeft.x + mAABB.halfSize.x * 2.0f - 2.0f, bottomLeft.y);

		for (XMFLOAT2 checkedTile = bottomLeft; ;checkedTile.x += map->mTileSize)
		{
			checkedTile.x = min(checkedTile.x, bottomRight.x);

			tileIndexX = map->GetMapTileXAtPoint(checkedTile.x);

			*groundY = (float)tileIndexY * map->mTileSize + map->mTileSize / 2.0f + map->mPosition.y;

			if (map->IsObstacle(tileIndexX, tileIndexY)) {

				mOnOneWayPlatformIs = false;
				return true;
			}

			else if (map->IsOneWayPlatform(tileIndexX, tileIndexY) && abs(checkedTile.y - *groundY) <= mOneWayPlatformThreshold + mOldPosition.y - position.y)
				mOnOneWayPlatformIs = true;


			if (checkedTile.x >= newBottomRight.x) {


				if (mOnOneWayPlatformIs)
					return true;
				break;
			}
		}

	}
	return false;
}

bool Entity2D::HasCeiling(XMFLOAT2 oldPosition, XMFLOAT2 position, XMFLOAT2 speed, float * ceilingY, Map * map)
{
	XMFLOAT2 oldCenter(oldPosition.x + mAABBOffset.x, oldPosition.y + mAABBOffset.y);
	XMFLOAT2 center(position.x + mAABBOffset.x, position.y + mAABBOffset.y);


	XMFLOAT2 oldTopRight(oldCenter.x + mAABB.halfSize.x - 1.0f, oldCenter.y + mAABB.halfSize.y - 1.0f);
	XMFLOAT2 newTopRight(center.x + mAABB.halfSize.x - 1.0f, center.y + mAABB.halfSize.y - 1.0f);
	XMFLOAT2 newTopLeft(newTopRight.x - mAABB.halfSize.x * 2.0f + 2.0f, newTopRight.y);


	int endY = map->GetMapTileYAtPoint(oldTopRight.y);
	int begY = max(map->GetMapTileYAtPoint(oldTopRight.y) + 1, endY);

	int dist = max(abs(endY - begY), 1);

	int tileIndexX;

	for (int tileIndexY = begY; tileIndexY <= endY; ++tileIndexY) {


		XMVECTOR topRightVec = XMVectorLerp(XMLoadFloat2(&newTopRight), XMLoadFloat2(&oldTopRight), (float)abs(endY - tileIndexY) / dist);
		XMFLOAT2 topRight;
		XMStoreFloat2(&topRight, topRightVec);

		XMFLOAT2 topLeft(topRight.x - mAABB.halfSize.x * 2.0f + 2.0f, topRight.y);

		for (XMFLOAT2 checkedTile = topLeft; ; checkedTile.x += map->mTileSize)
		{
			checkedTile.x = min(checkedTile.x, topRight.x);

			tileIndexX = map->GetMapTileXAtPoint(checkedTile.x);

			
			if (map->IsObstacle(tileIndexX, tileIndexY)) {

				*ceilingY = (float)tileIndexY * map->mTileSize - map->mTileSize / 2.0f + map->mPosition.y;
				return true;
			}

			
			if (checkedTile.x >= topRight.x) {
				break;
			}
		}

	}
	return false;
}

bool Entity2D::CollidesWithLeftWall(XMFLOAT2 oldPosition, XMFLOAT2 position, float * wallX, Map * map)
{
	XMFLOAT2 oldCenter(oldPosition.x + mAABBOffset.x, oldPosition.y + mAABBOffset.y);
	XMFLOAT2 center(position.x + mAABBOffset.x, position.y + mAABBOffset.y);


	XMFLOAT2 oldBottomLeft(oldCenter.x - mAABB.halfSize.x + 1.0f, oldCenter.y - mAABB.halfSize.y + 1.0f);
	XMFLOAT2 newBottomLeft(center.x - mAABB.halfSize.x + 1.0f, center.y - mAABB.halfSize.y + 1.0f); 
	XMFLOAT2 newTopLeft(newBottomLeft.x,center.y + mAABB.halfSize.y - 1.0f);



	int endx = map->GetMapTileXAtPoint(newBottomLeft.x);
	int begx = max(map->GetMapTileXAtPoint(oldBottomLeft.x) - 1, endx);

	int dist = max(abs(endx - begx), 1);

	int tileIndexY;

	if (begx <= 0.0f) {
		*wallX = 0.0f;
		return true;
	}

	for (int tileIndexX = begx; tileIndexX >= endx; --tileIndexX) {


		XMVECTOR bottomLeftVec = XMVectorLerp(XMLoadFloat2(&newBottomLeft), XMLoadFloat2(&oldBottomLeft), (float)abs(endx - tileIndexX) / dist);
		XMFLOAT2 bottomLeft;
		XMStoreFloat2(&bottomLeft, bottomLeftVec);

		XMFLOAT2 topLeft(bottomLeft.x,bottomLeft.y + mAABB.halfSize.y - 1.0f);

		for (XMFLOAT2 checkedTile = bottomLeft; ; checkedTile.y += map->mTileSize)
		{
			checkedTile.y = min(checkedTile.y, topLeft.y);

			tileIndexY = map->GetMapTileYAtPoint(checkedTile.y);


			if (map->IsObstacle(tileIndexX, tileIndexY)) {

				*wallX = (float)tileIndexX * map->mTileSize + map->mTileSize / 2.0f + map->mPosition.x;
				return true;
			}


			if (checkedTile.y >= topLeft.y) {
				break;
			}
		}

	}
	return false;
}

bool Entity2D::CollidesWithRightWall(XMFLOAT2 oldPosition, XMFLOAT2 position, float * wallX, Map * map)
{
	XMFLOAT2 oldCenter(oldPosition.x + mAABBOffset.x, oldPosition.y + mAABBOffset.y);
	XMFLOAT2 center(position.x + mAABBOffset.x, position.y + mAABBOffset.y);


	XMFLOAT2 oldBottomRight(oldCenter.x + mAABB.halfSize.x - 1.0f, oldCenter.y - mAABB.halfSize.y + 1.0f);
	XMFLOAT2 newBottomRight(center.x + mAABB.halfSize.x - 1.0f, center.y - mAABB.halfSize.y + 1.0f);
	XMFLOAT2 newTopLeft(newBottomRight.x, center.y + mAABB.halfSize.y - 1.0f);



	int endx = map->GetMapTileXAtPoint(newBottomRight.x);
	int begx = max(map->GetMapTileXAtPoint(oldBottomRight.x) + 1, endx);

	int dist = max(abs(endx - begx), 1);

	int tileIndexY;

	for (int tileIndexX = begx; tileIndexX <= endx; ++tileIndexX) {


		XMVECTOR bottomRightVec = XMVectorLerp(XMLoadFloat2(&newBottomRight), XMLoadFloat2(&oldBottomRight), (float)abs(endx - tileIndexX) / dist);
		XMFLOAT2 bottomRight;
		XMStoreFloat2(&bottomRight, bottomRightVec);

		XMFLOAT2 topRight(bottomRight.x, bottomRight.y + mAABB.halfSize.y - 1.0f);

		for (XMFLOAT2 checkedTile = bottomRight; ; checkedTile.y += map->mTileSize)
		{
			checkedTile.y = min(checkedTile.y, topRight.y);

			tileIndexY = map->GetMapTileYAtPoint(checkedTile.y);

			if (map->IsObstacle(tileIndexX, tileIndexY)) {

				*wallX = (float)((tileIndexX * map->mTileSize) - ( map->mTileSize / 2.0f)) + map->mPosition.x;
				return true;
			}


			if (checkedTile.y >= topRight.y) {
				break;
			}
		}

	}
	return false;
}
