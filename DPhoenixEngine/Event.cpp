#include "D3DUtil.h"


IEvent::IEvent()
{
	mId = 0;
}

IEvent::~IEvent()
{
}

KeyPressEvent::KeyPressEvent(int key)
{
	mId = EVENT_KEYPRESS;
	mKeycode = key;
}

KeyReleaseEvent::KeyReleaseEvent(int key)
{
	mId = EVENT_KEYRELEASE;
	mKeycode = key;
}

MouseClickEvent::MouseClickEvent(int btn, int x, int y)
{
	mId = EVENT_MOUSECLICK;
	mButton = btn;
	mPosX = x; mPosY = y;
}

MouseMotionEvent::MouseMotionEvent(int dx, int dy)
{
	mId = EVENT_MOUSEMOTION;
	mDeltaX = dx;
	mDeltaY = dy;

}

MouseMoveEvent::MouseMoveEvent(int x, int y)
{
	mId = EVENT_MOUSEMOVE;
	mPosX = x;
	mPosY = y;
}

MouseWheelEvent::MouseWheelEvent(int wheel)
{
	mId = EVENT_MOUSEWHEEL;
	mWheel = wheel;
}

XboxEvent::XboxEvent(XINPUT_STATE padState, XBoxPad* padPtr)
{
	mId = EVENT_XBOX_INPUT;
	mPadState = padState;
	mPadPointer = padPtr;

}
