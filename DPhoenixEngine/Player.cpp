#include "Player.h"




Player::Player(TextureManager * mTexMgr, ID3D11Device * md3dDevice, AudioManager * mAudioMgr, int conNum)
{
	playerNum = conNum;
#pragma region LoadSprites

	mIdleSprite = new Sprite();
	mIdleSprite->Load("Textures\\Characters\\Player\\pIdle.png", mTexMgr, 16, 16, md3dDevice);
	mIdleSprite->SetCurrentFrame(0); mIdleSprite->mAnimationColumns = 4;
	mIdleSprite->mAnimationDirection = 12; mIdleSprite->SetAnimationRange(0, 3);
	mIdleSprite->SetScale(1.0f); mIdleSprite->mFlipValue = 1.0f;

	mStandDFSprite = new Sprite();
	mStandDFSprite->Load("Textures\\Characters\\Player\\pIdleFireDown.png", mTexMgr, 16, 16, md3dDevice);
	mStandDFSprite->SetCurrentFrame(0); mStandDFSprite->mAnimationColumns = 4;
	mStandDFSprite->mAnimationDirection = 12; mStandDFSprite->SetAnimationRange(0, 3);
	mStandDFSprite->SetScale(1.0f); mStandDFSprite->mFlipValue = 1.0f;

	mStandUFSprite = new Sprite();
	mStandUFSprite->Load("Textures\\Characters\\Player\\pIdleFireUp.png", mTexMgr, 16, 16, md3dDevice);
	mStandUFSprite->SetCurrentFrame(0); mStandUFSprite->mAnimationColumns = 4;
	mStandUFSprite->mAnimationDirection = 12; mStandUFSprite->SetAnimationRange(0, 3);
	mStandUFSprite->SetScale(1.0f); mStandUFSprite->mFlipValue = 1.0f;

	mRunSprite = new Sprite();
	mRunSprite->Load("Textures\\Characters\\Player\\pWalk.png", mTexMgr, 16, 16, md3dDevice);
	mRunSprite->SetCurrentFrame(0); mRunSprite->mAnimationColumns = 4;
	mRunSprite->mAnimationDirection = 12; mRunSprite->SetAnimationRange(0, 3);
	mRunSprite->SetScale(1.0f); mRunSprite->mFlipValue = 1.0f;

	mRunUFSprite = new Sprite();
	mRunUFSprite->Load("Textures\\Characters\\Player\\pWalkFireUp.png", mTexMgr, 16, 16, md3dDevice);
	mRunUFSprite->SetCurrentFrame(0); mRunUFSprite->mAnimationColumns = 4;
	mRunUFSprite->mAnimationDirection = 12; mRunUFSprite->SetAnimationRange(0, 3);
	mRunUFSprite->SetScale(1.0f); mRunUFSprite->mFlipValue = 1.0f;

	mRunDFSprite = new Sprite();
	mRunDFSprite->Load("Textures\\Characters\\Player\\pWalkFireDown.png", mTexMgr, 16, 16, md3dDevice);
	mRunDFSprite->SetCurrentFrame(0); mRunDFSprite->mAnimationColumns = 4;
	mRunDFSprite->mAnimationDirection = 12; mRunDFSprite->SetAnimationRange(0, 3);
	mRunDFSprite->SetScale(1.0f); mRunDFSprite->mFlipValue = 1.0f;

	mJumpSprite = new Sprite();
	mJumpSprite->Load("Textures\\Characters\\Player\\pJump.png", mTexMgr, 16, 16, md3dDevice);
	mJumpSprite->SetCurrentFrame(0); mJumpSprite->mAnimationColumns = 4;
	mJumpSprite->mAnimationDirection = 12; mJumpSprite->SetAnimationRange(0, 3);
	mJumpSprite->SetScale(1.0f); mJumpSprite->mFlipValue = 1.0f;

	mPlayerNumSprite = new Sprite();
	mPlayerNumSprite->Load("Textures\\Characters\\Player\\pIcons.png", mTexMgr, 10, 10, md3dDevice);
	mPlayerNumSprite->SetCurrentFrame(playerNum-1); mPlayerNumSprite->mAnimationColumns = 2;
	mPlayerNumSprite->mAnimationDirection = 0; mPlayerNumSprite->SetAnimationRange(playerNum-1, playerNum-1);
	mPlayerNumSprite->SetScale(1.0f); mPlayerNumSprite->mFlipValue = 1.0f;
	
#pragma endregion


	for (int i = 0; i < P_MAX_INPUT; i++)
	{
		mInputs[i] = false;
		mPrevInputs[i] = false;
	}

	mMoveState		= P_STAND_MOVE;
	mActionState	= P_NOTFIRING_ACTION;
	mLifeState		= P_OK_LIFE;

	isFacingLeft = false;
	isHoldingFlag = false;

	//Init player stats here


	//AABB and parent class init
	mPosition.x = 0.0f; mPosition.y = 0.0f;
	mAABB.center.x = 0.0f; mAABB.center.y = 0.0f;
	mAABB.halfSize.x = 3.0f; mAABB.halfSize.y = 8.0f;
	mAABBOffset.x = 0.0f; mAABBOffset.y = 0.0f;


	//Colliders flags
	mPushRightWallJust = false;	mPushRightWallIs = false;
	mPushLeftWallJust = false;	mPushLeftWallIs = false;

	mOnGroundIs = false; mOnGroundJust = false;
	mAtCeilingIs = false; mAtCeilingJust = false;

	mOneWayPlatformThreshold = 2.0f;
	mOnOneWayPlatformIs = false;

	mCurrentSprite = mIdleSprite;
	//Start any timers here



}

void Player::PlayerUpdate(float dt, Map* map)
{


	switch (mLifeState) {
	case P_OK_LIFE:
		switch (mMoveState) {

		case P_STAND_MOVE:
			mSpeed.x = 0.0f;
			mSpeed.y = 0.0f;

			if (!mOnGroundIs) {
				mMoveState = P_JUMP_MOVE;
				break;
			}

			if (KeyState(P_FIRE_INPUT)) {

				mActionState = P_FIRING_ACTION;

				if (KeyState(P_RIGHT_INPUT))
				{
					mCurrentSprite = mIdleSprite;
					isFacingLeft = false;
				}
				else if (KeyState(P_LEFT_INPUT))
				{
					mCurrentSprite = mIdleSprite;
					isFacingLeft = true;
				}
			}
			else
			{
				mActionState = P_NOTFIRING_ACTION;
				mCurrentSprite = mIdleSprite;

				if (KeyState(P_RIGHT_INPUT) != KeyState(P_LEFT_INPUT)) {
					mMoveState = P_RUN_MOVE;
				}
				else if (KeyState(P_JUMP_INPUT) && KeyState(P_DOWN_INPUT)) {
					
					if (mOnOneWayPlatformIs)
						mPosition.y -= mOneWayPlatformThreshold;
				}
				else if (KeyState(P_JUMP_INPUT)) {
					mSpeed.y = mJumpForce;
					mMoveState = P_JUMP_MOVE;
				}
			}
			break;
		case P_RUN_MOVE:

			if (KeyState(P_RIGHT_INPUT) == KeyState(P_LEFT_INPUT)) {
				mMoveState = P_STAND_MOVE;
				mSpeed.x = 0.0f; mSpeed.y = 0.0f;
				break;
			}
			else if (KeyState(P_RIGHT_INPUT)) {

				if (mPushRightWallIs)
					mSpeed.x = 0.0f;
				else
					mSpeed.x = mRunSpeed;

				//Firing Check

				if (KeyState(P_FIRE_INPUT)) {

					mActionState = P_FIRING_ACTION;
					mCurrentSprite = mRunFSprite;
				}
				else
				{
					mActionState = P_NOTFIRING_ACTION;
					mCurrentSprite = mRunSprite;
				}

				isFacingLeft = false;
			}
			else if (KeyState(P_LEFT_INPUT)) {

				if (mPushLeftWallIs)
					mSpeed.x = 0.0f;
				else
					mSpeed.x = -mRunSpeed;

				//Firing Check

				if (KeyState(P_FIRE_INPUT)) {

					mActionState = P_FIRING_ACTION;
					mCurrentSprite = mRunFSprite;
				}
				else
				{
					mActionState = P_NOTFIRING_ACTION;
					mCurrentSprite = mRunSprite;
				}

				isFacingLeft = true;

				mCurrentSprite->mAnimationDirection = 12;
			}

			if (KeyState(P_JUMP_INPUT)) {

				mSpeed.y = mJumpForce;

				mMoveState = P_JUMP_MOVE;
				break;
			}
			else if (!mOnGroundIs) {
				mMoveState = P_JUMP_MOVE;
			}



			break;
		case P_JUMP_MOVE: {

			mCurrentSprite = mIdleSprite;

			mSpeed.y += mGravity * dt;
			mSpeed.y = max(mSpeed.y, mFallMaxSpeed);


			if (!KeyState(P_JUMP_INPUT) && mSpeed.y > 0.0f) {
				mSpeed.y = min(mSpeed.y, mJumpMinForce);
			}


			if (KeyState(P_RIGHT_INPUT) == KeyState(P_LEFT_INPUT)) {
				mSpeed.x = 0.0f;


				if (KeyState(P_FIRE_INPUT)) {
					mActionState = P_FIRING_ACTION;
					mCurrentSprite = mJumpSprite;
				}
				else {
					mActionState = P_NOTFIRING_ACTION;

					mCurrentSprite = mJumpSprite;
				}

			}
			else if (KeyState(P_RIGHT_INPUT)) {

				if (mPushRightWallIs)
					mSpeed.x = 0.0f;
				else
					mSpeed.x = mRunSpeed;

				isFacingLeft = false;
				//Firing Check

				if (KeyState(P_FIRE_INPUT)) {

					mActionState = P_FIRING_ACTION;
					mCurrentSprite = mJumpSprite;
				}
				else
				{
					mActionState = P_NOTFIRING_ACTION;
					mCurrentSprite = mJumpSprite;
				}

			}
			else if (KeyState(P_LEFT_INPUT)) {

				if (mPushLeftWallIs)
					mSpeed.x = 0.0f;
				else
					mSpeed.x = -mRunSpeed;

				isFacingLeft = true;
				//Firing Check

				if (KeyState(P_FIRE_INPUT)) {

					mActionState = P_FIRING_ACTION;
					mCurrentSprite = mJumpSprite;
				}
				else
				{
					mActionState = P_NOTFIRING_ACTION;
					mCurrentSprite = mJumpSprite;
				}

			}

			if (mOnGroundIs) {

				if (mInputs[(int)P_RIGHT_INPUT] == KeyState(P_LEFT_INPUT)) {

					mMoveState = P_STAND_MOVE;
					mSpeed.x = 0.0f; mSpeed.y = 0.0f;
				}

				else {


					mMoveState = P_RUN_MOVE;
					mSpeed.y = 0.0f;
				}
			}

		}
		break;
		}
	}

		UpdatePhysics(dt, map);

		UpdatePrevInputs();

		mCurrentSprite->Update(dt);
		mPlayerNumSprite->Update(dt);

		if (isFacingLeft)
			mCurrentSprite->mFlipValue = -1.0f;
		else
			mCurrentSprite->mFlipValue = 1.0f;


		mPlayerNumSprite->mPosition.y = mPosition.y + 15;
		mPlayerNumSprite->mPosition.x = mPosition.x;
		mCurrentSprite->mPosition.x = mPosition.x;
		mCurrentSprite->mPosition.y = mPosition.y;

	
}

bool Player::Released(PlayerInput input)
{
	return (!mInputs[(int)input] && mPrevInputs[(int)input]);
}

bool Player::KeyState(PlayerInput input)
{
	return (mInputs[(int)input]);
}

bool Player::Pressed(PlayerInput input)
{
	return (mInputs[(int)input] && !mPrevInputs[(int)input]);
}

void Player::UpdatePrevInputs()
{

	for (int i = 0; i < P_MAX_INPUT; i++)
	{
		mPrevInputs[i] = mInputs[i];

		mInputs[i] = false;
	}



}
