#include "TestScene.h"



TestScene::TestScene(HINSTANCE hInstance, std::string name) : 
	Scene(hInstance,name)
{
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd) {

#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	TestScene app(hInstance, "Astro-Babies");



	if (!app.Init(false)) {
		return 0;
	}



	return app.Run();


}


bool TestScene::Init(bool fullscreen)
{
	Scene::Init(fullscreen);

	LoadContent();

	Scene::BuildGeometryBuffers();
	mGameState = GAME_PLAY_STATE;

	return true;
}

void TestScene::LoadContent()
{
	Scene::LoadContent();


	mMap = new Map("Levels\\TestLevel.csv", &mTexMgr, md3dDevice,22,14,1,1);


	mPlayerOne = new Player(&mTexMgr, md3dDevice, &mAudioMgr, 1);
	mPlayerOne->mPosition = mMap->mPlayerOneSpawn;
	mPlayerTwo = new Player(&mTexMgr, md3dDevice, &mAudioMgr, 2);
	mPlayerTwo->mPosition = mMap->mPlayerOneSpawn;


	float tileAdjust = mMap->mTileSize / 2;
	mCamera = new Camera2D();
	mCamera->mPosition = mPlayerOne->mPosition;
	//Horizontal
	mCamera->mLeftLimit = (mMap->mPosition.x + mClientWidth / 2) - tileAdjust;
	mCamera->mRightLimit = mCamera->mLeftLimit + mMap->width *mMap->mTileSize - mClientWidth;
	//Vertical
	mCamera->mBottomLimit = (mMap->mPosition.y + mClientHeight / 2) - tileAdjust;

	mCamera->mTopLimit = mCamera->mBottomLimit + mMap->height *mMap->mTileSize - mClientHeight;

}

float x;
float y;
void TestScene::UpdateScene(float dt)
{
	Scene::UpdateScene(dt);


	switch (mGameState)
	{
	case GAME_TITLE_STATE:
		break;
	case GAME_PLAY_STATE:
		mPlayerOne->PlayerUpdate(dt, mMap);
		mPlayerTwo->PlayerUpdate(dt, mMap);

		x = (mPlayerOne->mPosition.x + mPlayerTwo->mPosition.x)/2;
		y = (mPlayerOne->mPosition.y + mPlayerTwo->mPosition.y)/2;
		mCamera->mPosition = XMFLOAT2(x, y);
		mCamera->Update(dt, mClientWidth, mClientHeight);


		break;
	case GAME_PAUSE_STATE:
		break;
	case GAME_WIN_STATE:
		break;
	case GAME_LOSE_STATE:
		break;
	}

}

void TestScene::DrawScene()
{
	Scene::DrawScene();


	md3dImmediateContext->OMSetDepthStencilState(mDepthDisableStencilState, 1);
	//Draw Sprites here	

 #pragma region RenderSprites

	
	switch (mGameState)
	{
	case GAME_TITLE_STATE:
		break;
	case GAME_PLAY_STATE:
		RenderMap(activeSpriteTech);


		RenderSprite(mPlayerOne->mCurrentSprite, activeSpriteTech);
		RenderSprite(mPlayerOne->mPlayerNumSprite, activeSpriteTech); 
		RenderSprite(mPlayerTwo->mCurrentSprite, activeSpriteTech);
		RenderSprite(mPlayerTwo->mPlayerNumSprite, activeSpriteTech);



		break;
	case GAME_PAUSE_STATE:
		break;
	case GAME_WIN_STATE:
		break;
	case GAME_LOSE_STATE:
		break;
	default:
		break;
	}

	md3dImmediateContext->OMSetDepthStencilState(mDepthStencilState, 1);
		
		
#pragma endregion
		
#pragma region Font
		
	std::ostringstream debugText;
		
	debugText << mPlayerOne->mPosition.x << std::endl;
	debugText << mPlayerOne->mPosition.y << std::endl;
	debugText << mPlayerOne->mSpeed.x<< std::endl;
		
	mDebugFont->DrawFont(m2D1RT, debugText.str(), 10.0f, 500.0f, 1.0f, 1.0f, D2D1::ColorF(0xFFFFFF, 0.5f));
		
#pragma endregion
		
	HRESULT hr = mSwapChain->Present(0, 0);
		
			// If the device was reset we must completely reinitialize the renderer.
	if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
	{
		MessageBox(0, "D3 has been lost! Please Restart till fix has been found!.", 0, 0);
		PostQuitMessage(0);
	}
		

}

void TestScene::RenderMap(ID3DX11EffectTechnique * tech)
{
	float scaleX = mScreenScale.x;
	float scaleY = mScreenScale.y;


	XMFLOAT3 scaleVec; scaleVec.x = scaleX;
	scaleVec.y = scaleY; scaleVec.z = 1.0f;

	XMMATRIX viewProj = mCamera->GetViewProj(XMFLOAT2(1.0f, 1.0f));

	int colBegin = mMap->GetMapTileYAtPoint(mCamera->mPosition.y - mClientHeight / 2);
	int colEnd = mMap->GetMapTileYAtPoint(mCamera->mPosition.y + mClientHeight / 2)+1;

	int rowBegin = mMap->GetMapTileXAtPoint(mCamera->mPosition.x - mClientWidth / 2);
	int rowEnd = mMap->GetMapTileXAtPoint(mCamera->mPosition.x + mClientWidth / 2) + 1;

	if (rowBegin < 0) rowBegin = 0; if (colBegin < 0) colBegin = 0;
	if (rowEnd > mMap->width) rowEnd = mMap->width;
	if (colEnd > mMap->height) colEnd = mMap->height;


	for (int row = rowBegin; row < rowEnd; row++)
	{
		for (int col = colBegin; col < colEnd; col++)
		{		

			if (mMap->mTiles[row][col].type == VOID_TILE)
				continue;

			if (mMap->mTiles[row][col].type == BLOCK_TILE) {
				Effects::SpriteFX->SetDiffuseMap(mMap->mSoildTileSprite->GetShaderResourceView());
			}
			else if (mMap->mTiles[row][col].type == ONEWAY_TILE) {
				Effects::SpriteFX->SetDiffuseMap(mMap->mOneWayTileSprite->GetShaderResourceView());
			}


			//Set tile position
			XMFLOAT2 tilePos = mMap->GetMapTilePosition(row, col);
			mMap->mSoildTileSprite->mPosition.x = tilePos.x;
			mMap->mSoildTileSprite->mPosition.y = tilePos.y;

			XMMATRIX testScale = XMMatrixScaling(5.0f, 5.0f, 1.0f);


			XMMATRIX world = mMap->mSoildTileSprite->CalculateTransforms(scaleVec);
			XMMATRIX worldViewProj = XMMatrixMultiply(world, viewProj);
			Effects::SpriteFX->SetWorldViewProj(worldViewProj * testScale);

			Effects::SpriteFX->SetColumn(mMap->GetTile(row, col).bitMaskValue);
			Effects::SpriteFX->SetNumCols(mMap->mSoildTileSprite->mAnimationColumns);
			Effects::SpriteFX->SetFlipValue(mMap->mSoildTileSprite->mFlipValue);

			//Use technieq to render
			D3DX11_TECHNIQUE_DESC techDesc;
			tech->GetDesc(&techDesc);
			for (UINT p = 0; p < techDesc.Passes; ++p) {
				tech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
				mMap->mSoildTileSprite->Render(md3dImmediateContext);
			}

		}
	}


}

void TestScene::HandleEvents(IEvent * e)
{

	switch (e->GetID())
	{
	case IEvent::EVENT_KEYPRESS:

		KeyPressEvent * kpe = (KeyPressEvent*)e;

		switch (kpe->mKeycode)
		{
		case DIK_D:
			mPlayerOne->mInputs[(int)P_RIGHT_INPUT] = true;
			break;
		case DIK_A:
			mPlayerOne->mInputs[(int)P_LEFT_INPUT] = true;
			break;
		case DIK_W:
			mPlayerOne->mInputs[(int)P_UP_INPUT] = true;
			break;
		case DIK_S:
			mPlayerOne->mInputs[(int)P_DOWN_INPUT] = true;
			break;
		case DIK_SPACE:
			mPlayerOne->mInputs[(int)P_JUMP_INPUT] = true;
			break;

		case DIK_UP:
			mPlayerTwo->mInputs[(int)P_UP_INPUT] = true;

			break;
		case DIK_RIGHT:
			mPlayerTwo->mInputs[(int)P_RIGHT_INPUT] = true;
			break;
		case DIK_LEFT:
			mPlayerTwo->mInputs[(int)P_LEFT_INPUT] = true;
			break;
		case DIK_DOWN:
			mPlayerTwo->mInputs[(int)P_DOWN_INPUT] = true;
			break;
		case DIK_RSHIFT:

			mPlayerTwo->mInputs[(int)P_JUMP_INPUT] = true;
			break;

		}




		break;
	}


}



TestScene::~TestScene()
{
	Effects::DestoryAll();
	InputLayouts::DestoryAll();
}

