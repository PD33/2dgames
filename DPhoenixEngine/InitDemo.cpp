//#include "Engine.h"
//#include "Event.h"
//
//class InitDemo : public DPhoenix::Engine
//{
//public:
//	InitDemo(HINSTANCE hInstance);
//	~InitDemo();
//
//	bool Init(bool fullscreen);
//
//	void OnResize();
//	void UpdateScene(float dt);
//	void DrawScene();
//
//	void BuildGeometryBuffers();
//
//	 
//
//private:
//
//	//Depth stencil
//	ID3D11DepthStencilState * mDepthStencilState;
//	ID3D11DepthStencilState* mDepthDisableStencilState;
//	D3D11_DEPTH_STENCIL_DESC mDepthStencilDesc;
//	D3D11_DEPTH_STENCIL_DESC mDepthDisableStencilDesc;
//
//	//vertex buffer and index buffer
//	ID3D11Buffer* mSpriteVB;
//	ID3D11Buffer* mSpriteIB;
//
//	//TextureManager
//	TextureManager mTexMgr;
//
//
//	AudioManager mAudioMgr;
//
//	//sprite shader resource view pointer
//	ID3D11ShaderResourceView* mSpriteTexSRV;
//
//	//sprite position
//	XMFLOAT3 position;
//	XMFLOAT3 velocity;
//
//
//	//world, view and projection matrices
//	XMFLOAT4X4 mWorld;
//	XMFLOAT4X4 mView;
//	XMFLOAT4X4 mProj;
//
//	//offset and counter vars
//	int mSpriteVertexOffset;
//	UINT mSpriteIndexOffset;
//	UINT mSpriteIndexCount;
//
//
//	// Inherited via Engine
//	virtual void HandleEvents(IEvent * e) override;
//
//	void InitAudio();
//};
//
//
//
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd) {
//
//#if defined(DEBUG) | defined(_DEBUG)
//	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
//#endif
//
//	InitDemo app(hInstance);
//
//
//
//	if (!app.Init(false)) {
//		return 0;
//	}
//
//
//
//	return app.Run();
//
//
//}
//
//
//InitDemo::InitDemo(HINSTANCE hInstance) : Engine(hInstance)
//{
//
//
//	mMainWndCaption = "Input Demo";
//}
//
//
//InitDemo::~InitDemo()
//{
//	ReleaseCOM(mSpriteVB);
//	ReleaseCOM(mSpriteIB);
//
//
//	Effects::DestoryAll();
//	InputLayouts::DestoryAll();
//
//
//}
//
//bool InitDemo::Init(bool fullscreen)
//{
//
//	if (!Engine::Init(fullscreen))
//		return false;
//
//	//Set up the texture mananager;
//	mTexMgr.Init(md3dDevice);
//
//
//	Effects::InitAll(md3dDevice);
//	InputLayouts::InitAll(md3dDevice);
//
//	SoundLayer::Create(mhMainWnd);
//	InitAudio();
//
//	//Create the texutre manager and store pointer
//	mSpriteTexSRV = mTexMgr.CreateTexture("Textures\\DeanyP\\JazzHands.png");
//
//
//	//Build quads for sprites
//	BuildGeometryBuffers();
//
//	//set the position of the sprite	
//	position.x = 5.0f;	position.y = 5.0f;	position.z = 0.0f;
//	velocity.x = 0.0f;	velocity.y = 0.0f;	velocity.z = 0.0f;
//
//
//	return true;
//}
//
//void InitDemo::InitAudio() {
//
//	mAudioMgr.CreateSound("TestOne", "Audio\\Music\\IgnoranceTitle.wav");
//
//}
//
//
//void InitDemo::OnResize()
//{
//
//	Engine::OnResize();
//
//}
//
//void InitDemo::UpdateScene(float dt)
//{
//
//	position.x += velocity.x * dt;
//	position.y += velocity.y * dt;
//
//
//	//reset the velocity
//	velocity.x = 0.0f; velocity.y = 0.0f;
//
//
//}
//
//void InitDemo::DrawScene()
//{
//
//
//	assert(md3dImmediateContext);
//	assert(mSwapChain);
//
//	ID3DX11EffectTechnique* activeSpriteTech = Effects::SpriteFX->SpriteTect;
//
//
//	float clearColour[4] = { 0.0f,0.2f,0.4f,1.0f };
//	md3dImmediateContext->ClearRenderTargetView(mRenderTargetView, clearColour);
//	md3dImmediateContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
//
//	//Set up the vertex input layout
//	md3dImmediateContext->IASetInputLayout(InputLayouts::SpritePosTex);
//	
//	md3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//
//	//RenderSprites
//
//	md3dImmediateContext->OMSetDepthStencilState(mDepthDisableStencilState, 1);
//
//	UINT stride = sizeof(Vertex::SpritePosTex);
//
//	UINT offset = 0;
//
//	md3dImmediateContext->IASetVertexBuffers(0, 1, &mSpriteVB, &stride, &offset);
//
//	md3dImmediateContext->IASetIndexBuffer(mSpriteIB, DXGI_FORMAT_R32_UINT, 0);
//
//	//Identity matrix
//	XMMATRIX view = XMMatrixIdentity();
//
//	//how much to scale on the screen
//	float scaleX = (float)mClientWidth / (1440 / 4); //The / 4 is the scale factor so it scales with the screen size This will be replaced by a sigle float variable making it easier to scale
//	float scaleY = (float)mClientHeight / (900 / 4); // The / 4 is the scale factor so it scales with the screen size
//
//	XMFLOAT3 scaleVec; scaleVec.x = scaleX;
//	scaleVec.y = scaleY;; scaleVec.z = 1.0f;
//
//	//Orthographic, left handed coords;
//	XMMATRIX projection = XMMatrixOrthographicLH((float)mClientWidth, (float)mClientHeight, 0.1f, 100.0f);
//
//
//	XMMATRIX viewProj = XMMatrixMultiply(view, projection);
//
//	//Set constants
//	//Translate
//	XMMATRIX translate = XMMatrixTranslation(position.x, position.y, position.z);
//	XMMATRIX scale = XMMatrixScaling(scaleVec.x, scaleVec.y, scaleVec.z);
//	XMMATRIX rotate = XMMatrixRotationZ(0.0f);
//	XMMATRIX world = translate * rotate * scale;
//
//	XMMATRIX worldViewProj = XMMatrixMultiply(world, viewProj);
//
//	Effects::SpriteFX->SetWorldViewProj(worldViewProj);
//	Effects::SpriteFX->SetDiffuseMap(mSpriteTexSRV);
//
//	D3DX11_TECHNIQUE_DESC techDesc;
//	activeSpriteTech->GetDesc(&techDesc);
//
//	for (UINT p = 0; p < techDesc.Passes; ++p) {
//		
//		//use current context
//		activeSpriteTech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
//
//		//^ indices for quad
//		md3dImmediateContext->DrawIndexed(6, 0, 0);
//
//
//	}
//
//	md3dImmediateContext->OMSetDepthStencilState(mDepthStencilState, 1);
//
//#pragma region 3DRenderering
//
//
//
//#pragma endregion 3DRenderering
//
//
//	HRESULT hr = mSwapChain->Present(0, 0);
//
//	// If the device was reset we must completely reinitialize the renderer.
//	if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
//	{
//		MessageBox(0, "D3 has been lost! Please Restart till fix has been found!.", 0, 0);
//		PostQuitMessage(0);
//	}
//
//
//}
//
//
//
//
////Other stuff this need to be refactored into the engine as this seems to be unessory
//
//void InitDemo::BuildGeometryBuffers() {
//
//	//width and height of the quad
//	float halfWidth = 4.0f;
//	float halfHeight= 4.0f;
//
//	// Needs fixing
//	Vertex::SpritePosTex vertices[] =
//	{
//		//A
//		{ XMFLOAT3(halfWidth, halfHeight, 1.0f), XMFLOAT2(1.0f,0.0f) },
//		//B
//
//		{ XMFLOAT3(halfWidth, -halfHeight, 1.0f), XMFLOAT2(1.0f,1.0f) },
//		//C
//		{ XMFLOAT3(-halfWidth, -halfHeight, 1.0f), XMFLOAT2(0.0f,1.0f) },
//		//D
//		{ XMFLOAT3(-halfWidth, halfHeight, 1.0f), XMFLOAT2(0.0f,0.0f) },
//
//	};
//
//	//Creat he vertex buffer
//	D3D11_BUFFER_DESC vbd;
//	vbd.Usage = D3D11_USAGE_IMMUTABLE;
//	vbd.ByteWidth = sizeof(Vertex::SpritePosTex) * 4;
//	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vbd.CPUAccessFlags = 0;
//	vbd.MiscFlags = 0;
//	vbd.StructureByteStride = 0;
//
//	//Create subresoruces with vertiex buffer data
//	D3D11_SUBRESOURCE_DATA vinitData;
//	//Apply vertices array
//	vinitData.pSysMem = vertices;
//
//	//Create buffer from devcies
//	HR(md3dDevice->CreateBuffer(&vbd, &vinitData,&mSpriteVB));
//	
//	//Create the index buffer
//	UINT indices[] = {
//
//		0, 1, 2,
//		0, 2, 3,
//	};
//
//	//Create index buffer subresource
//	D3D11_BUFFER_DESC ibd;
//	ibd.Usage = D3D11_USAGE_IMMUTABLE;
//	ibd.ByteWidth = sizeof(UINT) * 6;
//	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	ibd.CPUAccessFlags = 0;
//	ibd.MiscFlags = 0;
//	ibd.StructureByteStride = 0;
//
//	//Create the subresource
//	D3D11_SUBRESOURCE_DATA iinitData;
//	//apply indices array
//	iinitData.pSysMem = indices;
//
//	HR(md3dDevice->CreateBuffer(&ibd, &iinitData, &mSpriteIB));
//
//
//
//	//Create the blend state
//
//
//
//	D3D11_BLEND_DESC blendDesc;
//	ZeroMemory(&blendDesc, sizeof(blendDesc));
//	blendDesc.RenderTarget[0].BlendEnable = TRUE;
//	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
//	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
//	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
//	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
//
//	//This can affect the blend/transparecny
//	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
//
//	//Blend stat
//	ID3D11BlendState* alphaBlendState;
//
//	md3dDevice->CreateBlendState(&blendDesc, &alphaBlendState);
//	md3dImmediateContext->OMSetBlendState(alphaBlendState, 0, 0xffffffff);
//
//	//Depth stencial states;
//
//	ZeroMemory(&mDepthStencilBuffer, sizeof(mDepthStencilBuffer));
//
//
//	//Enable depth checking
//	mDepthStencilDesc.DepthEnable = true;
//
//	//Use full buffer for depth checks
//	mDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	mDepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
//
//
//	mDepthStencilDesc.StencilEnable = true;
//	mDepthStencilDesc.StencilReadMask = 0xff;
//	mDepthStencilDesc.StencilWriteMask = 0xff;
//
//	//Stencil operations operations if pixel is front-facing
//	mDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//
//	//Increment if stencil and depth
//	mDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	//Keep if stencil test passes
//	mDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	//Always compare data agaisnt existing
//	mDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	//sTENCIL OPERATIONS IF PIXEL IS BACKFACEING
//	
//	//Stencil operations operations if pixel is back-facing
//	mDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//
//	//Increment if stencil and depth
//	mDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	//Keep if stencil test passes
//	mDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	//Always compare data agaisnt existing
//	mDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	HRESULT result = md3dDevice->CreateDepthStencilState(&mDepthStencilDesc, &mDepthStencilState);
//
//	if (FAILED(result)) {
//
//
//	}
//
//	//Set the depth stencil state
//	md3dImmediateContext->OMSetDepthStencilState(mDepthStencilState, 1);
//
//	//Clear the memory
//	ZeroMemory(&mDepthDisableStencilDesc, sizeof(mDepthDisableStencilDesc));
//
//	//Now create the second depth stencil state which turns off the z buffer for 2D renderering
//
//	mDepthDisableStencilDesc.DepthEnable = false;
//	mDepthDisableStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	mDepthDisableStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
//	mDepthDisableStencilDesc.StencilEnable = true;
//	mDepthDisableStencilDesc.StencilReadMask = 0xFF;
//	mDepthDisableStencilDesc.StencilWriteMask = 0xFF;
//
//	mDepthDisableStencilDesc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.FrontFace.StencilDepthFailOp	= D3D11_STENCIL_OP_INCR;
//	mDepthDisableStencilDesc.FrontFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
//	mDepthDisableStencilDesc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.BackFace.StencilFailOp			= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_DECR;
//	mDepthDisableStencilDesc.BackFace.StencilPassOp			= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
//
//
//	result = md3dDevice->CreateDepthStencilState(&mDepthDisableStencilDesc, &mDepthDisableStencilState);
//
//	if (FAILED(result)) {
//		//return false;
//	}
//
//
//}
//
//void InitDemo::HandleEvents(IEvent * e)
//{
//
//	switch (e->GetID())
//	{
//	case IEvent::EVENT_KEYPRESS:
//
//		KeyPressEvent* kpe = (KeyPressEvent*)e;
//		switch (kpe->mKeycode)
//		{
//			case DIK_D: // Right
//				velocity.x += 20.0f;
//				mAudioMgr.GetSound("TestOne")->Play(true);
//				break;
//			case DIK_A: // Left
//				velocity.x += -20.0f;
//				break;
//			case DIK_W: // Up
//				velocity.y += 20.0f;
//				break;
//			case DIK_S: // Down
//				velocity.y += -20.0f;
//				break;
//			case DIK_ESCAPE:
//				PostQuitMessage(0);
//				break;
//		}
//
//
//		break;
//	}
//
//
//}
