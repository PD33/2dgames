#pragma once

#include "D3DUtil.h"
class Sound;


class SoundLayer 
{

private:
	LPDIRECTSOUND8 mDSound;
	LPDIRECTSOUNDBUFFER mPrimary;
	static SoundLayer* mGlobalSLayer;
	SoundLayer(HWND hWnd);

public:
	virtual ~SoundLayer();
	static SoundLayer* GetSound() {

		return mGlobalSLayer;
	}

	//Getter for directsound pointer
	LPDIRECTSOUND8 GetDSound() {

		return mDSound;
	}

	static void Create(HWND hWnd) {

		new SoundLayer(hWnd);
	}

	static SoundLayer* SoundPtr() {
		return SoundLayer::GetSound();
	}











};
