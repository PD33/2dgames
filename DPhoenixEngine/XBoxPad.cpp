#include "D3DUtil.h"


XBoxPad::XBoxPad(int playerNumber)
{
	mControllerNum = playerNumber - 1;

}


XBoxPad::~XBoxPad()
{
}

XINPUT_STATE XBoxPad::GetState()
{
	//Zeroise the state
	ZeroMemory(&mControllerState, sizeof(XINPUT_STATE));

	XInputGetState(mControllerNum, &mControllerState);

	return mControllerState;
}

bool XBoxPad::IsConnected()
{
	ZeroMemory(&mControllerState, sizeof(XINPUT_STATE));

	DWORD result = XInputGetState(mControllerNum, &mControllerState);

	if (result == ERROR_SUCCESS) {

		return true;
	}
	else
	{
		return false;
	}
}

void XBoxPad::Vibrate(int leftVal, int rightVal)
{

	XINPUT_VIBRATION Vibration;

	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	Vibration.wLeftMotorSpeed = leftVal;
	Vibration.wRightMotorSpeed = rightVal;


	XInputSetState(mControllerNum, &Vibration);

}
