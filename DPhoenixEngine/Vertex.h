#pragma once
#include "D3DUtil.h"


namespace Vertex {

	//Pos floats for x,y,z position
	//Tex floats for uv texture co-ords
	struct SpritePosTex {

		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
	};

}


class InputLayoutDesc
{
public:
	//Init like const int a::a[4] = { 0, 1, 2, 3]; in .cpp file
	static const D3D11_INPUT_ELEMENT_DESC SpritePosTex[2];

};

class InputLayouts
{
public:

	static void InitAll(ID3D11Device* device);
	static void DestoryAll();

	static ID3D11InputLayout* SpritePosTex;

};