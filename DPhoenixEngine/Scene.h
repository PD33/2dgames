#pragma once
#include "Engine.h"
#include "Camera2D.h"

class Scene : public DPhoenix::Engine
{
public:
	Scene(HINSTANCE hInstance, std::string name);
	~Scene();

	virtual bool Init(bool fullscreen);
	virtual void LoadContent();

	virtual void UpdateScene(float dt);
	virtual void DrawScene();


	void OnResize();
	void BuildGeometryBuffers();


protected:
	//Depth stencil
	ID3D11DepthStencilState* mDepthStencilState;
	ID3D11DepthStencilState* mDepthDisableStencilState;
	D3D11_DEPTH_STENCIL_DESC mDepthStencilDesc;
	D3D11_DEPTH_STENCIL_DESC mDepthDisableStencilDesc;

	ID3DX11EffectTechnique* activeSpriteTech;

	//vertex buffer and index buffer
	ID3D11Buffer* mSpriteVB;
	ID3D11Buffer* mSpriteIB;

	//world, view and projection matrices
	XMFLOAT4X4 mWorld;
	XMFLOAT4X4 mView;
	XMFLOAT4X4 mProj;

	XMFLOAT2 mScreenScale;

	//offset and counter vars
	int mSpriteVertexOffset;
	UINT mSpriteIndexOffset;
	UINT mSpriteIndexCount;

	//Managaers
	TextureManager mTexMgr;
	AudioManager mAudioMgr;


	Font* mDebugFont;

	Camera2D* mCamera;

	// Inherited via Engine
	virtual void HandleEvents(IEvent * e) override;


	void RenderSprite(Sprite* sprite, ID3DX11EffectTechnique* tech, bool isHUD = false);

};

