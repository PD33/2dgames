#pragma once
#include "Scene.h"
#include "Player.h"
#include "Map.h"
#include "Camera2D.h"

enum GameState {
	GAME_TITLE_STATE,
	GAME_PLAY_STATE,
	GAME_PAUSE_STATE,
	GAME_WIN_STATE,
	GAME_LOSE_STATE,
};

class TestScene :
	public Scene
{
public:
	TestScene(HINSTANCE hInstance, std::string name);
	~TestScene();

	bool Init(bool fullscreen) override;

	void LoadContent();
	void UpdateScene(float dt) override;
	void DrawScene() override;

	void RenderMap(ID3DX11EffectTechnique* tech);

	void HandleEvents(IEvent* e) override;

private:

	GameState mGameState;
	Player* mPlayerOne;
	Player* mPlayerTwo;

	Map* mMap;


};

