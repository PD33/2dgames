#pragma once

#include "D3DUtil.h"
#include "Map.h"

struct AABB2D {


public:
	//properities for setting up box
	XMFLOAT2 center;
	XMFLOAT2 halfSize;

	AABB2D() {
		center.x = 0.0f;
		center.y = 0.0f;

		halfSize.x = 0.0f;
		halfSize.y = 0.0f;
	}

	AABB2D(XMFLOAT2 _center, XMFLOAT2 _halfSize) {

		center = _center;
		halfSize = _halfSize;
	}

	bool Overlaps(AABB2D other) {

		if (abs(center.x - other.center.x) > halfSize.x + other.halfSize.x)
			return false;


		if(abs(center.y - other.center.y) > halfSize.y + other.halfSize.y)
			return false;

		return true;
	}
};



class Entity2D
{
public:
	Entity2D();
	~Entity2D();

	//Entity Position
	XMFLOAT2 mOldPosition;
	XMFLOAT2 mPosition;

	//Entity speed
	XMFLOAT2 mOldSpeed;
	XMFLOAT2 mSpeed;
	//Scale value
	XMFLOAT2 mScale;

	//Collision
	AABB2D mAABB;
	XMFLOAT2 mAABBOffset;


	//Collision flags
	//Right
	bool mPushRightWallJust;
	bool mPushRightWallIs;
	//Left
	bool mPushLeftWallJust;
	bool mPushLeftWallIs;
	//Up
	bool mAtCeilingJust;
	bool mAtCeilingIs;
	//Down
	bool mOnGroundJust;
	bool mOnGroundIs;

	//for one way platforms
	float mOneWayPlatformThreshold;
	bool mOnOneWayPlatformIs;

	void UpdatePhysics(float dt, Map* map);	//Update the physics of the object
	bool HasGround(XMFLOAT2 oldPosition, XMFLOAT2 position, XMFLOAT2 speed, float* groundY, Map* map);
	bool HasCeiling(XMFLOAT2 oldPosition, XMFLOAT2 position, XMFLOAT2 speed, float* ceilingY, Map* map);
	bool CollidesWithLeftWall(XMFLOAT2 oldPosition, XMFLOAT2 position, float* wallX, Map* map);
	bool CollidesWithRightWall(XMFLOAT2 oldPosition, XMFLOAT2 position, float* wallX, Map* map);

};

