#pragma once

#include "Entity2D.h"

#pragma region States


enum PlayerInput {
	P_LEFT_INPUT,
	P_RIGHT_INPUT,
	P_UP_INPUT,
	P_DOWN_INPUT,
	P_JUMP_INPUT,
	P_FIRE_INPUT,
	P_PICKUP_INPUT,
	P_DROP_INPUT,
	P_MAX_INPUT
};


enum PlayerMoveState {

	P_STAND_MOVE,
	P_RUN_MOVE,
	P_JUMP_MOVE

};

enum PlayerActionState {

	P_FIRING_ACTION,
	P_NOTFIRING_ACTION,
	P_HOLDING_ACTION,

};

enum PlayerLifeState {

	P_OK_LIFE,
	P_HURT_LIFE,
	P_DIED_LIFE,


};

#pragma endregion



class Player : public Entity2D
{
public:
	Player(TextureManager* mTexMgr, ID3D11Device* md3dDevice,AudioManager* mAudioMgr, int conNum);
	~Player() {};

	Sprite* mDieSprite;
	Sprite* mIdleSprite;
	Sprite* mJumpSprite;
	Sprite* mRunSprite;
	Sprite* mHoldSprite;

	Sprite* mStandFSprite;
	Sprite* mStandDFSprite;
	Sprite* mStandUFSprite;
	Sprite* mRunFSprite;
	Sprite* mRunUFSprite;
	Sprite* mRunDFSprite;

	Sprite* mCurrentSprite;
	Sprite* mPlayerNumSprite;
	//Used for two player veirson
	int playerNum;

	float mJumpForce = 250.0f;
	float mRunSpeed = 200.0f;
	float mGravity = -100.0f;
	float mFallMaxSpeed = -200.0f;
	float mJumpMinForce = 100.0f;

	bool mInputs[P_MAX_INPUT];
	bool mPrevInputs[P_MAX_INPUT];

	bool isFacingLeft;
	bool isHoldingFlag;


	//Health stats
	int mHealth;
	int mMaxHealth;
	float mColorLerpValue;

	//States
	PlayerMoveState mMoveState;
	PlayerActionState mActionState;
	PlayerLifeState mLifeState;

	void PlayerUpdate(float dt, Map* map);

	bool Released(PlayerInput input);
	bool KeyState(PlayerInput input);
	bool Pressed(PlayerInput input);

	void UpdatePrevInputs();

};

