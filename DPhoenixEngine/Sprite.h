#pragma once
#include "D3DUtil.h"

class TextureManager;

class Sprite
{
protected:

	//Vertex and index buffers for the quad / sprite
	ID3D11Buffer * mSpriteVB; 
	ID3D11Buffer * mSpriteIB;

	//Texture (Shader resource view)
	ID3D11ShaderResourceView* mSpriteTexSRV;

public:
	XMFLOAT3 mPosition; //Position
	XMFLOAT3 mVelocity; //Velocity
	XMFLOAT3 mSize;		//Size
	int mAnimationColumns;
	float mCurrentFrame;
	int mFirstFrame;
	int mLastFrame;
	int mAnimationDirection;
	double mRotation;
	XMFLOAT3 mScale;
	XMFLOAT3 mPivotPoint;
	XMFLOAT2 mInitialPosition;
	XMFLOAT2 mParallax;


	float mFlipValue;
	float mOpacityValue;

	float mColorLerpValue;
	XMFLOAT4 mAltColorValue;

	Sprite();
	virtual ~Sprite();

	bool Load(std::string filename, TextureManager* mTexMgr, float width, float height, ID3D11Device* md3dDevice);


	RECT GetBounds();

	void Update(float deltaTime);
	void Render(ID3D11DeviceContext* dc);

	//Used to gerenate the amtrices for the renderering sprites
	XMMATRIX CalculateTransforms(XMFLOAT3 screenScale, bool isHUD = false);

	ID3D11ShaderResourceView* GetShaderResourceView() 
	{
		return mSpriteTexSRV;
	}

	void SetAnimationRange(int start, int end) {

		mFirstFrame = start;
		mLastFrame = end;

	}
	void SetScale(float horiz, float vert) {
		mScale.x = horiz;
		mScale.y = vert;
	}

	void SetScale(float scale) {
		SetScale(scale, scale);
	}


	void SetCurrentFrame(int frame) {

		mCurrentFrame = 0;
	}

	int GetCurrentFrame() 
	{

		//We need to round up and down to avoid glitching in the animation
		if (mAnimationDirection < 0) 
		{
			if (mCurrentFrame > mLastFrame)
				mCurrentFrame = mLastFrame;
			return (int)ceil(mCurrentFrame);

		}
		else
		{
			if (mCurrentFrame < mFirstFrame)
				mCurrentFrame = mFirstFrame;
			return (int)floor(mCurrentFrame);
		}



	}



};

