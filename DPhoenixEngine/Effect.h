#pragma once

#include "D3DUtil.h"



#pragma region Effect

class Effect
{
public:
	Effect(ID3D11Device* device, const std::string& fileName);
	virtual ~Effect();

private:
	Effect(const Effect& rhs);
	Effect& operator=(const Effect& rhs);


protected:
	ID3DX11Effect * mfx;

};
#pragma endregion


#pragma region SpriteEffect
class SpriteEffect : public Effect
{
public:
	SpriteEffect(ID3D11Device* device, const std::string& fileName);
	~SpriteEffect();


	//set the world projection combied matrix
	void SetWorldViewProj(CXMMATRIX M) { WorldViewProj->SetMatrix(reinterpret_cast<const float*>(&M)); }
	//set world matrix
	void SetWorld(CXMMATRIX M) { World->SetMatrix(reinterpret_cast<const float*>(&M)); }
	//Set diffuse map
	void SetDiffuseMap(ID3D11ShaderResourceView* tex) { SpriteTex->SetResource(tex); }
	//Set current column of spritesheet( for uv position)
	void SetColumn(float f) { Column->SetRawValue(&f, 0, sizeof(float)); }

	void SetNumCols(float f) { NumCols->SetRawValue(&f, 0, sizeof(float)); }
	//Set width of individual column (for uv mapping)
	void SetWidth(float f) { Width->SetRawValue(&f, 0, sizeof(float)); }

	//set wheather the sprite is flipped along x
	void SetFlipValue(float f) { FlipValue->SetRawValue(&f, 0, sizeof(float)); }

	void SetLerpValue(float f) { LerpValue->SetRawValue(&f, 0, sizeof(float)); }
	//set wheather the sprite to be interpoloted between
	void SetAltColourValue(const FXMVECTOR v) { AltColorValue->SetFloatVector(reinterpret_cast<const float*>(&v)); }
	//set an alternative tcolour to inerpolate between the texture width
	void SetOpacityValue(float f) { OpacityValue->SetRawValue(&f, 0, sizeof(float)); }



	//effect techiques that can be used
	ID3DX11EffectTechnique* SpriteTect;
	//matrix variables for the shader
	ID3DX11EffectMatrixVariable* WorldViewProj;
	ID3DX11EffectMatrixVariable* WorldViewProjTex;
	ID3DX11EffectMatrixVariable* World;

	//Float values for the shader
	ID3DX11EffectVariable* Column;
	ID3DX11EffectVariable* NumCols;
	ID3DX11EffectVariable* Width;
	ID3DX11EffectVariable* FlipValue;
	ID3DX11EffectVariable* LerpValue;
	ID3DX11EffectVariable* OpacityValue;

	//vector variables
	ID3DX11EffectVectorVariable* AltColorValue;

	//shader resource view - this is for the sprite texture;
	ID3DX11EffectShaderResourceVariable* SpriteTex;

private:

};


class Effects {

public:
	static void InitAll(ID3D11Device* device);
	static void DestoryAll();

	static SpriteEffect* SpriteFX;




};

