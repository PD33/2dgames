//#include "Engine.h"
//#include "Event.h"
//
//class SpriteDemo : public DPhoenix::Engine
//{
//public:
//	SpriteDemo(HINSTANCE hInstance);
//	~SpriteDemo();
//
//	bool Init(bool fullscreen);
//
//	void OnResize();
//	void UpdateScene(float dt);
//	void DrawScene();
//
//	void BuildGeometryBuffers();
//
//
//
//private:
//
//	//Depth stencil
//	ID3D11DepthStencilState * mDepthStencilState;
//	ID3D11DepthStencilState* mDepthDisableStencilState;
//	D3D11_DEPTH_STENCIL_DESC mDepthStencilDesc;
//	D3D11_DEPTH_STENCIL_DESC mDepthDisableStencilDesc;
//
//	//vertex buffer and index buffer
//	ID3D11Buffer* mSpriteVB;
//	ID3D11Buffer* mSpriteIB;
//
//	//TextureManager
//	TextureManager mTexMgr;
//
//	Sprite* mCharacterSprite;
//	Sprite* mCharacterSprite2;
//
//	Font* mDebugFont;
//	XMFLOAT2 mScreenScale;
//
//	AudioManager mAudioMgr;
//
//
//	//sprite position
//	XMFLOAT3 position;
//	XMFLOAT3 velocity;
//
//
//	//world, view and projection matrices
//	XMFLOAT4X4 mWorld;
//	XMFLOAT4X4 mView;
//	XMFLOAT4X4 mProj;
//
//	//offset and counter vars
//	int mSpriteVertexOffset;
//	UINT mSpriteIndexOffset;
//	UINT mSpriteIndexCount;
//
//
//	// Inherited via Engine
//	virtual void HandleEvents(IEvent * e) override;
//
//	void InitAudio();
//
//	void RenderSprite(Sprite* sprite, ID3DX11EffectTechnique* tech, bool isHUD = false);
//
//};
//
//
//
//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd) {
//
//#if defined(DEBUG) | defined(_DEBUG)
//	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
//#endif
//
//	SpriteDemo app(hInstance);
//
//
//
//	if (!app.Init(false)) {
//		return 0;
//	}
//
//
//
//	return app.Run();
//
//
//}
//
//
//SpriteDemo::SpriteDemo(HINSTANCE hInstance) : Engine(hInstance)
//{
//
//
//	mMainWndCaption = "Input Demo";
//}
//
//
//SpriteDemo::~SpriteDemo()
//{
//
//	Effects::DestoryAll();
//	InputLayouts::DestoryAll();
//
//
//}
//
//bool SpriteDemo::Init(bool fullscreen)
//{
//
//	if (!Engine::Init(fullscreen))
//		return false;
//
//	//Set up the texture mananager;
//	mTexMgr.Init(md3dDevice);
//
//
//	Effects::InitAll(md3dDevice);
//	InputLayouts::InitAll(md3dDevice);
//
//	SoundLayer::Create(mhMainWnd);
//	InitAudio();
//
//
//	//Build quads for sprites
//	BuildGeometryBuffers();
//	
//	//Create new animated sprite
//	mCharacterSprite = new Sprite();
//	mCharacterSprite->Load("Textures\\DeanyP\\JumpFDwn.png", &mTexMgr, 75.0f, 100.0f, md3dDevice);
//	mCharacterSprite->SetCurrentFrame(0); mCharacterSprite->mAnimationColumns = 4;
//	mCharacterSprite->mAnimationDirection = 16; mCharacterSprite->SetAnimationRange(0, 3);
//	mCharacterSprite->SetScale(1.0f); mCharacterSprite->mFlipValue = 1.0f;
//
//	//Create new animated sprite
//	mCharacterSprite2 = new Sprite();
//	mCharacterSprite2->Load("Textures\\DeanyP\\JazzHands.png", &mTexMgr, 75.0f, 100.0f, md3dDevice);
//	mCharacterSprite2->SetCurrentFrame(0); mCharacterSprite2->mAnimationColumns = 4;
//	mCharacterSprite2->mAnimationDirection = 16; mCharacterSprite2->SetAnimationRange(0,3);
//	mCharacterSprite2->SetScale(1.0f); mCharacterSprite2->mFlipValue = 1.0f;
//
//	//set the position of the sprite
//	position.x = 5.0f;	position.y = 5.0f;	position.z = 0.0f;
//	mCharacterSprite->mPosition = position;
//
//
//	velocity.x = 0.0f;	velocity.y = 0.0f;	velocity.z = 0.0f;
//
//	mDebugFont = new Font(mhMainWnd, mBlackBrush, mDWriteFactory, "Arial", 20.0f);
//
//	return true;
//}
//
//void SpriteDemo::InitAudio() {
//
//	mAudioMgr.CreateSound("TestOne", "Audio\\Music\\IgnoranceTitle.wav");
//
//}
//
//void SpriteDemo::RenderSprite(Sprite * sprite, ID3DX11EffectTechnique * tech, bool isHUD)
//{
//
//	float scaleX = mScreenScale.x;
//	float scaleY = mScreenScale.y;
//
//	XMFLOAT3 scaleVec; scaleVec.x = scaleX;
//	scaleVec.y = scaleY; scaleVec.z = 1.0f;
//
//
//	XMMATRIX viewProj;
//
//
//	if (!isHUD) {
//		//2D camera control
//	}
//	else
//	{
//		XMMATRIX view = XMMatrixIdentity();
//
//		XMMATRIX projection = XMMatrixOrthographicLH((float)mClientWidth, (float)mClientHeight, 0.1f, 100.0f);
//
//
//		viewProj = XMMatrixMultiply(view, projection);
//	}
//
//
//	XMMATRIX world = sprite->CalculateTransforms(scaleVec, isHUD);
//	XMMATRIX worldViewProj = XMMatrixMultiply(world, viewProj);
//	Effects::SpriteFX->SetWorldViewProj(worldViewProj);
//
//	Effects::SpriteFX->SetDiffuseMap(sprite->GetShaderResourceView());
//	Effects::SpriteFX->SetColumn(sprite->GetCurrentFrame());
//	Effects::SpriteFX->SetNumCols(sprite->mAnimationColumns);
//	Effects::SpriteFX->SetFlipValue(sprite->mFlipValue);
//	Effects::SpriteFX->SetLerpValue(sprite->mColorLerpValue);
//	Effects::SpriteFX->SetOpacityValue(sprite->mOpacityValue);
//	Effects::SpriteFX->SetAltColourValue(XMLoadFloat4(&sprite->mAltColorValue));
//
//
//	D3DX11_TECHNIQUE_DESC techDesc;
//
//	tech->GetDesc(&techDesc);
//
//	for (UINT p = 0; p < techDesc.Passes; ++p)
//	{
//		tech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
//
//		sprite->Render(md3dImmediateContext);
//	}
//
//
//	Effects::SpriteFX->SetLerpValue(0.0f);
//
//
//
//}
//
//
//void SpriteDemo::OnResize()
//{
//
//	Engine::OnResize();
//
//}
//
//void SpriteDemo::UpdateScene(float dt)
//{
//
//	mScreenScale.x = (float)mClientWidth / 1440;
//	mScreenScale.y = (float)mClientHeight / 900;
//
//
//
//	mCharacterSprite->mVelocity = velocity;
//
//	mCharacterSprite->Update(dt);
//	//reset the velocity
//	velocity.x = 0.0f; velocity.y = 0.0f;
//
//
//}
//
//void SpriteDemo::DrawScene()
//{
//
//
//	assert(md3dImmediateContext);
//	assert(mSwapChain);
//
//	ID3DX11EffectTechnique* activeSpriteTech = Effects::SpriteFX->SpriteTect;
//
//
//	float clearColour[4] = { 0.0f,0.2f,0.4f,1.0f };
//	md3dImmediateContext->ClearRenderTargetView(mRenderTargetView, clearColour);
//	md3dImmediateContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
//
//	//Set up the vertex input layout
//	md3dImmediateContext->IASetInputLayout(InputLayouts::SpritePosTex);
//
//	md3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//
//	//Clear the rendertarget
//	//m2D1RT->BeginDraw();
//
//	//m2D1RT->Clear(D2D1::ColorF(0x00000000));
//
//	//m2D1RT->EndDraw();
//
//#pragma region 3DRenderering
//
//
//
//#pragma endregion 3DRenderering
//
//#pragma region RenderSprites
//
//	md3dImmediateContext->OMSetDepthStencilState(mDepthDisableStencilState, 1);
//
//	RenderSprite(mCharacterSprite, activeSpriteTech, true);
//	RenderSprite(mCharacterSprite2, activeSpriteTech, true);
//
//	md3dImmediateContext->OMSetDepthStencilState(mDepthStencilState, 1);
//
//
//#pragma endregion
//
//#pragma region Font
//
//	std::ostringstream debugText;
//
//	debugText << "Test String" << std::endl;
//
//	//mDebugFont->DrawFont(m2D1RT, debugText.str(), 10.0f, 500.0f, 1.0f, 1.0f, D2D1::ColorF(0xFFFFFF, 0.5f));
//
//#pragma endregion
//
//	HRESULT hr = mSwapChain->Present(0, 0);
//
//	// If the device was reset we must completely reinitialize the renderer.
//	if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
//	{
//		MessageBox(0, "D3 has been lost! Please Restart till fix has been found!.", 0, 0);
//		PostQuitMessage(0);
//	}
//
//
//}
//
//
//
//
////Other stuff this need to be refactored into the engine as this seems to be unessory
//
//void SpriteDemo::BuildGeometryBuffers() {
//
//
//	//Create the blend state
//	D3D11_BLEND_DESC blendDesc;
//	ZeroMemory(&blendDesc, sizeof(blendDesc));
//	blendDesc.RenderTarget[0].BlendEnable = TRUE;
//	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
//	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
//	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
//	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
//	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
//
//	//This can affect the blend/transparecny
//	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;;
//
//	//Blend stat
//	ID3D11BlendState* alphaBlendState_;
//	
//	md3dDevice->CreateBlendState(&blendDesc, &alphaBlendState_);
//	md3dImmediateContext->OMSetBlendState(alphaBlendState_, 0, 0xffffffff);
//
//	//Depth stencial states;
//
//	ZeroMemory(&mDepthStencilBuffer, sizeof(mDepthStencilBuffer));
//
//
//	//Enable depth checking
//	mDepthStencilDesc.DepthEnable = true;
//
//	//Use full buffer for depth checks
//	mDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	//Use full buffer for depth checks
//	mDepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
//	
//
//	mDepthStencilDesc.StencilEnable = true;
//	mDepthStencilDesc.StencilReadMask = 0xff;
//	mDepthStencilDesc.StencilWriteMask = 0xff;
//
//	//Stencil operations operations if pixel is front-facing
//	mDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	//Increment if stencil and depth
//	mDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	//Keep if stencil test passes
//	mDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	//Always compare data agaisnt existing
//	mDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	//sTENCIL OPERATIONS IF PIXEL IS BACKFACEING
//	
//	//Stencil operations operations if pixel is back-facing
//	mDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//
//	//Increment if stencil and depth
//	mDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//	//Keep if stencil test passes
//	mDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	//Always compare data agaisnt existing
//	mDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	HRESULT result = md3dDevice->CreateDepthStencilState(&mDepthStencilDesc, &mDepthStencilState);
//
//	if (FAILED(result)) {
//
//		debug << "Fail";
//	}
//
//	//Set the depth stencil state
//	md3dImmediateContext->OMSetDepthStencilState(mDepthStencilState, 1);
//
//	//Clear the memory
//	ZeroMemory(&mDepthDisableStencilDesc, sizeof(mDepthDisableStencilDesc));
//
//	//Now create the second depth stencil state which turns off the z buffer for 2D renderering
//
//	mDepthDisableStencilDesc.DepthEnable			= false;
//	mDepthDisableStencilDesc.DepthWriteMask			= D3D11_DEPTH_WRITE_MASK_ALL;
//	mDepthDisableStencilDesc.DepthFunc				= D3D11_COMPARISON_LESS;
//	mDepthDisableStencilDesc.StencilEnable			= true;
//	mDepthDisableStencilDesc.StencilReadMask		= 0xFF;
//	mDepthDisableStencilDesc.StencilWriteMask		= 0xFF;
//
//	mDepthDisableStencilDesc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.FrontFace.StencilDepthFailOp	= D3D11_STENCIL_OP_INCR;
//	mDepthDisableStencilDesc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.FrontFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
//	mDepthDisableStencilDesc.BackFace.StencilFailOp			= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_DECR;
//	mDepthDisableStencilDesc.BackFace.StencilPassOp			= D3D11_STENCIL_OP_KEEP;
//	mDepthDisableStencilDesc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
//
//
//	result = md3dDevice->CreateDepthStencilState(&mDepthDisableStencilDesc, &mDepthDisableStencilState);
//
//	if (FAILED(result)) {
//		//return false;
//
//		debug << "Fail";
//	}
//
//
//}
//
//
//void SpriteDemo::HandleEvents(IEvent* e)
//{
//
//	switch (e->GetID())
//	{
//	case IEvent::EVENT_KEYPRESS:
//
//		KeyPressEvent * kpe = (KeyPressEvent*)e;
//		switch (kpe->mKeycode)
//		{
//		case DIK_D: // Right
//			velocity.x += 20.0f;
//			mCharacterSprite->mFlipValue = 1.0f;
//			mCharacterSprite->mAnimationDirection = 16;
//			break;
//		case DIK_A: // Left
//			velocity.x += -20.0f;
//			mCharacterSprite->mFlipValue = -1.0f;
//			mCharacterSprite->mAnimationDirection = -16;
//			break;
//		case DIK_W: // Up
//			velocity.y += 20.0f;
//			break;
//		case DIK_S: // Down
//			velocity.y += -20.0f;
//			break;
//		case DIK_ESCAPE:
//			PostQuitMessage(0);
//			break;
//		}
//
//
//		break;
//	}
//
//
//}
