#include "Input.h"





Input::Input(HWND window)
{

	//save the window handle
	mWindow = window;

	//Creating the directinput object
	DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&mDi, NULL);

	mDi->CreateDevice(GUID_SysKeyboard, &mKeyboard, NULL);

	//Settting up the keyboard ------------------------------------------

	//Settting the data format to standard
	mKeyboard->SetDataFormat(&c_dfDIKeyboard);

	//We do this so it's doesn't stop use from using other windows
	mKeyboard->SetCooperativeLevel(mWindow, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	mKeyboard->Acquire();

	//Clear the array that contains the keys
	memset(mKeyState, 0, 256);

	//Settting up the mouse ------------------------------------------

	mDi->CreateDevice(GUID_SysMouse, &mMouse, NULL);

	//Mouse data format
	mMouse->SetDataFormat(&c_dfDIMouse);
	//We do this so it's doesn't stop use from using other windows
	mMouse->SetCooperativeLevel(mWindow, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	mMouse->Acquire();


}



void Input::Update()
{
	//Poll the state of the keyboard // Assuming this gets the current keyboard state
	mKeyboard->Poll();


	if (!SUCCEEDED(mKeyboard->GetDeviceState(256, (LPVOID)&mKeyState))) {

		//So if we have lost connection to the keyboard attempt to get it back
		mKeyboard->Acquire();
	}

	//Poll the state of the mouse // Assuming this gets the current mouse state
	mMouse->Poll();

	if (!SUCCEEDED(mKeyboard->GetDeviceState(sizeof(DIMOUSESTATE), &mMouseState))) {

		//So if we have lost connection to the keyboard attempt to get it back
		mMouse->Acquire();
	}

	//set mousePoint to the current mouse position on screen;
	GetCursorPos(&mMousePoint);

	//Get position relative to window
	ScreenToClient(mWindow, &mMousePoint);


}



Input::~Input()
{

	mMouse->Release();		//Release mouse
	mKeyboard->Release();	//Release Keyboard
	mDi->Release();			//Release direct input object

}
