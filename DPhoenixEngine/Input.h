#pragma once
#include "D3DUtil.h"
class Input
{
public:
	Input(HWND window);
	virtual ~Input();

	void Update();

	//getters and setters
	//Keyboard inputs
	char GetKeyState(int key) { return mKeyState[key]; }

	//Mouse inputs
	int GetMouseButton(char button) { return mMouseState.rgbButtons[button] & 0x80; }

	//get mouse pos x & y
	long GetMousePosX() { return mMousePoint.x; }
	long GetMousePosY() { return mMousePoint.y; }

	//How far has the mouse moved along the respective axies since the last update
	long GetMouseDeltaX() { return mMouseState.lX; }
	long GetMouseDeltaY() { return mMouseState.lY; }

	//How much the scroll wheel has moved since the last update
	long GetMouseDeltaWheel() { return mMouseState.lZ; }


private:
	HWND mWindow;						// Window Handle

	IDirectInput8* mDi;					//Direct input device
	IDirectInputDevice8* mKeyboard;		//Keyboard	
	char mKeyState[256];				//key state array

	IDirectInputDevice8* mMouse;		//Mouse
	DIMOUSESTATE mMouseState;			//Mouse state
	POINT mMousePoint;					//x, y cursor position

};

