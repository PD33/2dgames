#include "Scene.h"



Scene::Scene(HINSTANCE hInstance, std::string name) : Engine(hInstance)
{
	mMainWndCaption = name;
}





Scene::~Scene()
{
	Effects::DestoryAll();
	InputLayouts::DestoryAll();
}

bool Scene::Init(bool fullscreen)
{
	if (!Engine::Init(fullscreen))
		return false;

	//Set up the texture mananager;
	mTexMgr.Init(md3dDevice);

	//Set up effects and input layouts
	Effects::InitAll(md3dDevice);
	InputLayouts::InitAll(md3dDevice);

	//Create the sound layer
	SoundLayer::Create(mhMainWnd);


	//Build quads for sprites
	BuildGeometryBuffers();

	return true;
}

void Scene::LoadContent()
{
	//Here we should load in anything that will be used on all scene types

	//Set debug font
	mDebugFont = new Font(mhMainWnd, mBlackBrush, mDWriteFactory, "Arial", 20.0f);

}

void Scene::UpdateScene(float dt)
{
	mScreenScale.x = (float)((mClientWidth / 1440));
	mScreenScale.y = (float)((mClientHeight/ 900) );



}

void Scene::DrawScene()
{
	//Call this in the child class so we can have a much cleaner child class

	assert(md3dImmediateContext);
	assert(mSwapChain);


	activeSpriteTech = Effects::SpriteFX->SpriteTect;
	//Call the rendering sprite and font drawing in the sub class


	float clearColour[4] = { 0.0f,0.2f,0.4f, 1.0f };
	md3dImmediateContext->ClearRenderTargetView(mRenderTargetView, clearColour);
	md3dImmediateContext->ClearDepthStencilView(mDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//Set up the vertex input layout
	md3dImmediateContext->IASetInputLayout(InputLayouts::SpritePosTex);

	md3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Clear the rendertarget
	m2D1RT->BeginDraw();

	//m2D1RT->Clear(D2D1::ColorF(0x0000000000));

	m2D1RT->EndDraw();

}

void Scene::OnResize()
{
	Engine::OnResize();
}

void Scene::BuildGeometryBuffers()

{//Create the blend state
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	//This can affect the blend/transparecny
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;;

	//Blend stat
	ID3D11BlendState* alphaBlendState_;

	md3dDevice->CreateBlendState(&blendDesc, &alphaBlendState_);
	md3dImmediateContext->OMSetBlendState(alphaBlendState_, 0, 0xffffffff);

	//Depth stencial states;

	ZeroMemory(&mDepthStencilBuffer, sizeof(mDepthStencilBuffer));


	//Enable depth checking
	mDepthStencilDesc.DepthEnable = true;

	//Use full buffer for depth checks
	mDepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	//Use full buffer for depth checks
	mDepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;


	mDepthStencilDesc.StencilEnable = true;
	mDepthStencilDesc.StencilReadMask = 0xff;
	mDepthStencilDesc.StencilWriteMask = 0xff;

	//Stencil operations operations if pixel is front-facing
	mDepthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	//Increment if stencil and depth
	mDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	//Keep if stencil test passes
	mDepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	//Always compare data agaisnt existing
	mDepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//sTENCIL OPERATIONS IF PIXEL IS BACKFACEING

	//Stencil operations operations if pixel is back-facing
	mDepthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;

	//Increment if stencil and depth
	mDepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	//Keep if stencil test passes
	mDepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	//Always compare data agaisnt existing
	mDepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	HRESULT result = md3dDevice->CreateDepthStencilState(&mDepthStencilDesc, &mDepthStencilState);

	if (FAILED(result)) {

		debug << "Fail";
	}

	//Set the depth stencil state
	md3dImmediateContext->OMSetDepthStencilState(mDepthStencilState, 1);

	//Clear the memory
	ZeroMemory(&mDepthDisableStencilDesc, sizeof(mDepthDisableStencilDesc));

	//Now create the second depth stencil state which turns off the z buffer for 2D renderering

	mDepthDisableStencilDesc.DepthEnable = false;
	mDepthDisableStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	mDepthDisableStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	mDepthDisableStencilDesc.StencilEnable = true;
	mDepthDisableStencilDesc.StencilReadMask = 0xFF;
	mDepthDisableStencilDesc.StencilWriteMask = 0xFF;

	mDepthDisableStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthDisableStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	mDepthDisableStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mDepthDisableStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	mDepthDisableStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	mDepthDisableStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	mDepthDisableStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	mDepthDisableStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;


	result = md3dDevice->CreateDepthStencilState(&mDepthDisableStencilDesc, &mDepthDisableStencilState);

	if (FAILED(result)) {
		//return false;

		debug << "Fail";
	}

}

void Scene::HandleEvents(IEvent * e)
{
	//Place anything to do with input that is used on every scene here


}

void Scene::RenderSprite(Sprite * sprite, ID3DX11EffectTechnique * tech, bool isHUD)
{

	
	float scaleX = mScreenScale.x;
	float scaleY = mScreenScale.y;
	
	XMFLOAT3 scaleVec; scaleVec.x = scaleX;
	scaleVec.y = scaleY; scaleVec.z = 1.0f;	
	
	XMMATRIX viewProj;
	
	if (!isHUD) {
		//2D camera control
		viewProj = mCamera->GetViewProj(sprite->mParallax);
	}
	else
	{
		XMMATRIX view = XMMatrixIdentity();
		XMMATRIX projection = XMMatrixOrthographicLH((float)mClientWidth, (float)mClientHeight, 0.1f, 100.0f);
		
		viewProj = XMMatrixMultiply(view, projection);
	}
	
	
	XMMATRIX world = sprite->CalculateTransforms(scaleVec, isHUD);
	XMMATRIX worldViewProj = XMMatrixMultiply(world, viewProj);			
	XMMATRIX testScale = XMMatrixScaling(5.0f, 5.0f, 1.0f);


	Effects::SpriteFX->SetWorldViewProj(worldViewProj * testScale);
	
	Effects::SpriteFX->SetDiffuseMap(sprite->GetShaderResourceView());
	Effects::SpriteFX->SetColumn(sprite->GetCurrentFrame());
	Effects::SpriteFX->SetNumCols(sprite->mAnimationColumns);
	Effects::SpriteFX->SetFlipValue(sprite->mFlipValue);
	Effects::SpriteFX->SetLerpValue(sprite->mColorLerpValue);
	Effects::SpriteFX->SetOpacityValue(sprite->mOpacityValue);
	Effects::SpriteFX->SetAltColourValue(XMLoadFloat4(&sprite->mAltColorValue));
	
	
	D3DX11_TECHNIQUE_DESC techDesc;
	
	tech->GetDesc(&techDesc);
	
	for (UINT p = 0; p < techDesc.Passes; ++p)
	{
		tech->GetPassByIndex(p)->Apply(0, md3dImmediateContext);
		sprite->Render(md3dImmediateContext);
	}
	
	
	Effects::SpriteFX->SetLerpValue(0.0f);
	
	
	

}



