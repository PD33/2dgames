#pragma once


class XBoxPad
{
public:
	XBoxPad(int playerNumber);
	~XBoxPad();

	XINPUT_STATE GetState();

	bool IsConnected();

	void Vibrate(int leftVal = 0, int rightVal = 0);

	int GetPlayerNum() { return mControllerNum; }

private:
	XINPUT_STATE mControllerState;
	int mControllerNum;


};

