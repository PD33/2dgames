#include "Sprite.h"

Sprite::Sprite()
{

	//basic properties - pointers
	mSpriteVB = NULL;
	mSpriteIB = NULL;
	mSpriteTexSRV = NULL;

	mPosition.x = 0.0f; mPosition.y = 0.0f; mPosition.z = 0.0f;
	mInitialPosition.x = 0.0f; mInitialPosition.y = 0.0f;
	mVelocity.x = 0.0f; mVelocity.y = 0.0f; mVelocity.z = 0.0f;
	mSize.x = 1.0f; mSize.y = 1.0f; mSize.z = 1.0f;
	mAnimationColumns = 1;
	mCurrentFrame = 0;
	mFirstFrame = 0;
	mLastFrame = 0;
	mAnimationDirection = 1;
	mRotation = 0.0f;
	mScale.x = 1.0f; mScale.y = 1.0f; mScale.z = 1.0f;
	mPivotPoint.x = 0.0f; mPivotPoint.y = 0.0f;
	mPivotPoint.z = 0.0f;

	
	mFlipValue = 1.0f;
	mOpacityValue = 1.0f;

	mColorLerpValue = 0.0f;
	mAltColorValue = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

	mParallax.x = 1.0f;
	mParallax.y = 1.0f;

}

Sprite::~Sprite()
{

	ReleaseCOM(mSpriteVB);
	ReleaseCOM(mSpriteIB);
	ReleaseCOM(mSpriteTexSRV);
}

bool Sprite::Load(std::string filename, TextureManager * mTexMgr, float width, float height, ID3D11Device * md3dDevice)
{
	mSpriteTexSRV = mTexMgr->CreateTexture(filename);

	mSize.x = width;
	mSize.y = height;

	float halfWidth = mSize.x / 2;
	float halfHeight = mSize.y / 2;

	Vertex::SpritePosTex vertices[] =
	{
		//A
		{ XMFLOAT3(halfWidth, halfHeight, 1.0f), XMFLOAT2(1.0f,0.0f) },
		//B
		{ XMFLOAT3(halfWidth, -halfHeight, 1.0f), XMFLOAT2(1.0f,1.0f) },
		//C
		{ XMFLOAT3(-halfWidth, -halfHeight, 1.0f), XMFLOAT2(0.0f,1.0f) },
		//D
		{ XMFLOAT3(-halfWidth, halfHeight, 1.0f), XMFLOAT2(0.0f,0.0f) },
	};

	//Create the vertex buffer
	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.ByteWidth = sizeof(Vertex::SpritePosTex) * 4;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	//Create subresoruces with vertiex buffer data
	D3D11_SUBRESOURCE_DATA vinitData;
	//Apply vertices array
	vinitData.pSysMem = vertices;

	//Create buffer from devcies
	HR(md3dDevice->CreateBuffer(&vbd, &vinitData, &mSpriteVB));

	//Create the index buffer
	UINT indices[] = {

		0, 1, 2,
		0, 2, 3,
	};

	//Create index buffer subresource
	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.ByteWidth = sizeof(UINT) * 6;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;

	//Create the subresource
	D3D11_SUBRESOURCE_DATA iinitData;
	//apply indices array
	iinitData.pSysMem = indices;

	HR(md3dDevice->CreateBuffer(&ibd, &iinitData, &mSpriteIB));



	return true;
}

RECT Sprite::GetBounds()
{
	RECT rect;
	rect.left = mPosition.x;
	rect.top = mPosition.y;
	rect.right = mPosition.x + mSize.x * mScale.x;
	rect.bottom = mPosition.y + mSize.y * mScale.y;
	return rect;
}

void Sprite::Update(float deltaTime)
{
	mCurrentFrame += (float)((float)mAnimationDirection * deltaTime);


	if ((int)ceil(mCurrentFrame) < mFirstFrame)
		mCurrentFrame = (float)mLastFrame;
	if ((int)floor(mCurrentFrame) > mLastFrame)
		mCurrentFrame = (float)mFirstFrame;
}

void Sprite::Render(ID3D11DeviceContext * dc)
{

	//Calculate the size of individual data points
	UINT stride = sizeof(Vertex::SpritePosTex);
	UINT offset = 0;

	//Set the vertex buffer to the input assembler
	dc->IASetVertexBuffers(0, 1, &mSpriteVB, &stride, &offset);
	//Set index bufffer
	dc->IASetIndexBuffer(mSpriteIB, DXGI_FORMAT_R32_UINT, 0);

	dc->DrawIndexed(6, 0, 0);
}

XMMATRIX Sprite::CalculateTransforms(XMFLOAT3 screenScale, bool isHUD)
{

	//Set the scale of the sprite on x and y as vector
	XMVECTOR scaling = XMVectorSet(mScale.x * screenScale.x, mScale.y * screenScale.y, 1.0f, 0.0f);
	//Set origin of the sprite on x and y as vector
	XMVECTOR origin = XMVectorSet(mPivotPoint.x, mPivotPoint.y, 0.0f, 0.0f);

	//The translation is essentially positioning
	XMVECTOR translation;

	if (isHUD) {

		translation = XMVectorSet(mPosition.x, mPosition.y, 0.0f, 0.0f);
	}
	else 
	{
		translation = XMVectorSet(mPosition.x * screenScale.x, mPosition.y * screenScale.y, 0.0f, 0.0f);
	}


	XMMATRIX T = XMMatrixAffineTransformation2D(scaling, origin, mRotation, translation);

	return T;
}
