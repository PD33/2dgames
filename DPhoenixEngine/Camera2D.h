#pragma once

#include "D3DUtil.h"
enum Camera2DInput {

	C2D_LEFT_INPUT,
	C2D_RIGHT_INPUT,
	C2D_DOWN_INPUT,
	C2D_UP_INPUT,
	C2D_MAX_INPUT,
};


class Camera2D
{
public:
	Camera2D();
	~Camera2D();

	XMMATRIX mView;
	XMMATRIX mProjection;

	XMFLOAT2 mPosition;
	XMFLOAT2 mSpeed;

	XMFLOAT3 screenScale;

	//Bounds
	float mLeftLimit;
	float mRightLimit;
	float mTopLimit;
	float mBottomLimit;

	int mClientWidth;
	int mClientHeight;

	bool limitBounds = false;

	bool mInputs[C2D_MAX_INPUT];
	bool mPrevInputs[C2D_MAX_INPUT];


	void Update(float dt, int clientWidth, int clientHeight);
	XMMATRIX GetViewProj(XMFLOAT2 parallax);

	bool Released(Camera2DInput key);
	bool KeyState(Camera2DInput key);
	bool Pressed(Camera2DInput key);

	void UpdatePrevInputs();
};

