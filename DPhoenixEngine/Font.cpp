#include "Font.h"



Font::Font(HWND hWindow, ID2D1SolidColorBrush * brush, IDWriteFactory * writeFactory, std::string fontFamily, float size)
{
	mDWriteFactory = writeFactory;
	mFontFamily = fontFamily;
	mSize = size;
	mBrush = brush;
	mhWindow = hWindow;


	std::wstring fontString(mFontFamily.begin(), mFontFamily.end());

	HRESULT hr = mDWriteFactory->CreateTextFormat(fontString.c_str(), NULL, DWRITE_FONT_WEIGHT_REGULAR, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, size, L"en_us", &mTextFormat);

	if (SUCCEEDED(hr)) {

		hr = mTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	}
	if (SUCCEEDED(hr)) {

		hr = mTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
	}

}

Font::~Font()
{

	ReleaseCOM(mTextFormat);

}

void Font::DrawFont(ID2D1RenderTarget * _2DRT, std::string text, float x, float y, float scaleX, float scaleY, D2D1::ColorF color)
{

	std::wstring textString(text.begin(), text.end());

	UINT32 cTextLength_ = textString.length();

	RECT rc;
	GetClientRect(mhWindow, &rc);

	D2D1_RECT_F layoutRect = D2D1::RectF(
		static_cast<FLOAT>(rc.left + x),
		static_cast<FLOAT>(rc.top + y),
		static_cast<FLOAT>(rc.left + x * mSize * cTextLength_),
		static_cast<FLOAT>(rc.top + y * mSize)
	);

	_2DRT->CreateSolidColorBrush(D2D1::ColorF(color), &mBrush);

	_2DRT->BeginDraw();

	_2DRT->SetTransform(D2D1::Matrix3x2F::Scale(
		D2D1::Size(scaleX, scaleY),
		D2D1::Point2F(0.0f, 0.0f)));


	_2DRT->DrawText(
		textString.c_str(),
		cTextLength_,
		mTextFormat,
		layoutRect,
		mBrush
	);

	_2DRT->EndDraw();



}
