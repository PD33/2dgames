#pragma once

#include "D3DUtil.h"

class WaveSoundRead
{
public:
	WaveSoundRead();
	~WaveSoundRead();


	HRESULT Open(char* strFIlename);
	HRESULT Reset();
	HRESULT Read(UINT nSizeToRead, BYTE* pbData, UINT* pnSizeRead);
	HRESULT Close();

public:
	WAVEFORMATEX* mWfx;
	HMMIO mHmmioIn;
	MMCKINFO mCkIn;
	MMCKINFO mCkInRiff;


};


class Sound {

private:
	WaveSoundRead * mWaveSoundRead;
	LPDIRECTSOUNDBUFFER8 mBuffer;
	int mBufferSize;


	static std::map<WaveSoundRead*, int> mWaveMap;

	void Init();

public:

	//Constructors
	Sound(char* filename);
	Sound(Sound& in);
	Sound& operator=(const Sound &in);


	virtual ~Sound();

	//Main
	void Restore();
	void Fill();
	void Play(bool bLoop = false);
	bool IsPlaying();
	void Stop();
	void SetPosition(int pos);








};