#include "Vertex.h"
#include "Effects.h"


#pragma region InputLayoutDesc

const D3D11_INPUT_ELEMENT_DESC InputLayoutDesc::SpritePosTex[2] = {


	{"POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
	{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}


};

#pragma endregion



#pragma region InputLayouts



ID3D11InputLayout* InputLayouts::SpritePosTex = 0;

void InputLayouts::InitAll(ID3D11Device* device) {

	D3DX11_PASS_DESC passDesc;


	//SpritePosTex
	Effects::SpriteFX->SpriteTect->GetPassByIndex(0)->GetDesc(&passDesc);

	HR(device->CreateInputLayout(InputLayoutDesc::SpritePosTex, 2, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &SpritePosTex));



}

void InputLayouts::DestoryAll() {

	ReleaseCOM(SpritePosTex);
}

#pragma endregion