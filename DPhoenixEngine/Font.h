#pragma once

#include "D3DUtil.h"

class Font
{

private:
	IDWriteFactory* mDWriteFactory;
	IDWriteTextFormat* mTextFormat;
	ID2D1SolidColorBrush* mBrush;
	HWND mhWindow;

	std::string mFontFamily;
	float mSize;

public:

	Font(HWND hWindow, ID2D1SolidColorBrush* brush, IDWriteFactory* writeFactory, std::string fontFamily, float size);
	~Font();


	void DrawFont(ID2D1RenderTarget* _2DRT, std::string text, float x, float y, float scaleX, float scaleY, D2D1::ColorF color);
};

