#include "Map.h"



Map::Map(std::string filname, TextureManager * mTexMgr, ID3D11Device * md3dDevice, int tileWidthPerPanel, int tileHeightPerPanel, int amtPanelsWide, int amtPanelsHigh)
{


	mPosition.x = 0.0f;
	mPosition.x = 0.0f;


	mPlayerOneSpawn.x = 0.0f; mPlayerTwoSpawn.x = 0.0f;
	mPlayerOneSpawn.y = 0.0f; mPlayerTwoSpawn.y = 0.0f;


	std::ifstream inMapFile(filname);

	std::string value;
	std::string ignore;

	width = tileWidthPerPanel * amtPanelsWide;
	height = tileHeightPerPanel * amtPanelsHigh;

	mTileSize = 16;

	int row = height - 1;
	int col = 0;
	int count = 0;

	mTiles.resize(width);
	for (int i = 0; i < mTiles.size(); i++)
	{
		mTiles[i].resize(height);
	}


	//Map Parsing
	while (inMapFile.good()) {

		std::getline(inMapFile, value, '\n');

		if (value.length() > 1) {

			for (int i = 0; i < value.length(); i++)
			{
				int indexStart = (int)'0';
				int indexEnd = (int)'z';


				if ((int)value[i] >= indexStart && (int)value[i] <= indexEnd) {


					switch (value[i])
					{
					case '0':
						mTiles[col][row].type = VOID_TILE;
						break;
					case '1':
						mTiles[col][row].type = BLOCK_TILE;
						break;
					case '2':
						mTiles[col][row].type = ONEWAY_TILE;
						break;
					case 'd':
						mPlayerOneSpawn.x = mPosition.x + col * mTileSize;
						mPlayerOneSpawn.y = mPosition.y + row * mTileSize;
						break;
					}
					col++;
				}
			}
			row--; col = 0;
		}

		mSoildTileSprite = new Sprite();
		mSoildTileSprite->Load("Textures\\Level\\Sprites\\SoildBitmask.png", mTexMgr, 16, 16, md3dDevice);
		mSoildTileSprite->SetCurrentFrame(0); mSoildTileSprite->mAnimationColumns = 16;
		mSoildTileSprite->mAnimationDirection = 0; mSoildTileSprite->SetAnimationRange(0, 15);
		mSoildTileSprite->SetScale(1.0f); mSoildTileSprite->mFlipValue = 1.0f;

		mOneWayTileSprite = new Sprite();
		mOneWayTileSprite->Load("Textures\\Level\\Sprites\\SoildBitmask.png", mTexMgr, 16, 16, md3dDevice);
		mOneWayTileSprite->SetCurrentFrame(0); mOneWayTileSprite->mAnimationColumns = 16;
		mOneWayTileSprite->mAnimationDirection = 0; mOneWayTileSprite->SetAnimationRange(0, 15);
		mOneWayTileSprite->SetScale(1.0f); mOneWayTileSprite->mFlipValue = 1.0f;
		mOneWayTileSprite = new Sprite();

	}

	AssignBitmaskValues();

}

Map::~Map()
{
}

XMFLOAT2 Map::GetMapTileAtPoint(XMFLOAT2 point)
{
	int x;
	int y;
	x = (int)((point.x - mPosition.x + mTileSize / 2.0f) / (float)mTileSize);
	y = (int)((point.y - mPosition.y + mTileSize / 2.0f) / (float)mTileSize);

	return XMFLOAT2(x, y);
}

int Map::GetMapTileYAtPoint(float y)
{
	return (int)((y - mPosition.y + mTileSize / 2.0f) / (float)mTileSize);
}

int Map::GetMapTileXAtPoint(float x)
{
	return (int)((x - mPosition.x + mTileSize / 2.0f) / (float)mTileSize);
}

XMFLOAT2 Map::GetMapTilePosition(int tileIndexX, int tileIndexY)
{
	return XMFLOAT2((float)(tileIndexX * mTileSize) + mPosition.x, (float)(tileIndexY * mTileSize) + mPosition.y);
}

XMFLOAT2 Map::GetMapTilePosition(XMFLOAT2 tileCoords)
{
	return XMFLOAT2((float)(tileCoords.x * mTileSize) * mPosition.x, (float)(tileCoords.y * mTileSize) * mPosition.y);
}

BitMaskTile Map::GetTile(int x, int y)
{
	if (x < 0 || x >= width || y < 0 || y >= height) {
		BitMaskTile tile = BitMaskTile();
		tile.type = BLOCK_TILE;
		return tile;
	}

	return mTiles[x][y];
}

void Map::AssignBitmaskValues()
{

	for (int x = 0; x < mTiles.size(); x++)
	{
		for (int y = 0; y < mTiles[0].size(); y++)
		{

			mTiles[x][y].bitMaskValue = 0;

			TileTypes current = mTiles[x][y].type;

			if (!OutOfRange(x, y - 1, mTiles)) { 
				if (mTiles[x][y - 1].type == current) { 
					mTiles[x][y].bitMaskValue += 1; } }
			if (!OutOfRange(x + 1, y, mTiles)) { 
				if (mTiles[x + 1][y].type == current) { 
					mTiles[x][y].bitMaskValue += 2; } }
			if (!OutOfRange(x, y + 1, mTiles)) {
				if (mTiles[x][y + 1].type == current) { 
					mTiles[x][y].bitMaskValue += 4; } }
			if (!OutOfRange(x - 1, y, mTiles)) { 
				if (mTiles[x - 1][y].type == current) { 
					mTiles[x][y].bitMaskValue += 8; } }
			
		}
	}




}

bool Map::IsObstacle(int x, int y)
{
	if (x < 0 || x >= width || y < 0 || y >= height) {
		return true;
	}

	return (mTiles[x][y].type == BLOCK_TILE);
}

bool Map::IsGround(int x, int y)
{
	if (x < 0 || x >= width || y < 0 || y >= height) {
		return false;
	}

	return (mTiles[x][y].type == BLOCK_TILE || mTiles[x][y].type == ONEWAY_TILE);
}

bool Map::IsOneWayPlatform(int x, int y)
{
	if (x < 0 || x >= width || y < 0 || y >= height) {
		return false;
	}

	return (mTiles[x][y].type == ONEWAY_TILE);
}

bool Map::IsEmpty(int x, int y)
{
	if (x < 0 || x >= width || y < 0 || y >= height) {
		return false;
	}

	return (mTiles[x][y].type == VOID_TILE);
}
