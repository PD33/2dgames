#include "Camera2D.h"



Camera2D::Camera2D()
{
}


Camera2D::~Camera2D()
{
}

void Camera2D::Update(float dt, int clientWidth, int clientHeight)
{

	mClientWidth = clientWidth;
	mClientHeight = clientHeight;

	mSpeed.x = 0.0f; mSpeed.y = 0.0f;

	if (KeyState(C2D_LEFT_INPUT))
		mSpeed.x = -50.0f;
	else if (KeyState(C2D_RIGHT_INPUT))
		mSpeed.x = 50.0f;

	if (KeyState(C2D_UP_INPUT))
		mSpeed.y = 50.0f;
	else if (KeyState(C2D_DOWN_INPUT))
		mSpeed.y = -50.0f;


	mPosition.x += mSpeed.x * dt;
	mPosition.y += mSpeed.y * dt;

	if (limitBounds) {
		if (mPosition.x < mLeftLimit)
			mPosition.x = mLeftLimit;

		if (mPosition.x > mRightLimit)
			mPosition.x = mRightLimit;


		if (mPosition.y < mBottomLimit)
			mPosition.y = mBottomLimit;

		if (mPosition.y > mTopLimit)
			mPosition.y = mTopLimit;
	}

	UpdatePrevInputs();
}

XMMATRIX Camera2D::GetViewProj(XMFLOAT2 parallax)
{

	XMMATRIX mView = XMMatrixTranslation(-mPosition.x * parallax.x, -mPosition.y * parallax.y, 0.0f);

	XMMATRIX mProjection = XMMatrixOrthographicLH((float)mClientWidth, (float)mClientHeight, 0.1f, 100.0f);

	XMMATRIX viewProj = mView * mProjection;

	return viewProj;
}


bool Camera2D::Released(Camera2DInput input)
{
	return (!mInputs[(int)input] && mPrevInputs[(int)input]);
}

bool Camera2D::KeyState(Camera2DInput input)
{
	return (mInputs[(int)input]);
}

bool Camera2D::Pressed(Camera2DInput input)
{
	return (mInputs[(int)input] && !mPrevInputs[(int)input]);
}

void Camera2D::UpdatePrevInputs()
{

	for (int i = 0; i < C2D_MAX_INPUT; i++)
	{
		mPrevInputs[i] = mInputs[i];

		mInputs[i] = false;
	}



}
