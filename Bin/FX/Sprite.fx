
cbuffer cbPerObject {

	float4x4 gWorld;
	float4x4 gWorldViewProj;

	//float values;
	float gColumn = 0.0f;
	float gNumCols = 1.0f;
	float gFlipValue = 1.0f;
	float gLerpValue = 0.0f;
	float gOpacityValue = 1.0f; //MAKE SURE THIS IS 1.0F!


	//aternative colour (to lerp to)
	float4 gAltColor = float4(1.0f, 1.0f, 1.0f, 1.0f);

};

Texture2D gSpriteTex;


SamplerState samLinear {

	Filter = MIN_MAG_MIP_LINEAR; // Can use MIN_MAG_MIP_POINT
	AddressU = CLAMP;
	AddressV = CLAMP;

};

SamplerState samPoint {

	Filter = MIN_MAG_MIP_POINT; // Can use MIN_MAG_MIP_POINT
	AddressU = CLAMP;
	AddressV = CLAMP;

};

struct VertexShaderIn {

	float3 PosL : POSITION;
	float2 Tex	: TEXCOORD;

};
struct VertexShaderOut {

	float4 PosH : SV_POSITION;
	float2 Tex	: TEXCOORD0;

};


VertexShaderOut VS_Sprite(VertexShaderIn vertex) {

	VertexShaderOut vsOut = (VertexShaderOut)0;


	//create the struct
	float4 newPos;
	newPos.x = vertex.PosL.x; 
	newPos.y = vertex.PosL.y; 
	newPos.z = vertex.PosL.z;
	newPos.w = 1.0f;

	//mutliple 
	vsOut.PosH = mul(newPos, gWorldViewProj);

	//Work out the coloumsn 
	vsOut.Tex[0] = ((1.0f / gNumCols) * gColumn + vertex.Tex[0] / gNumCols);

	if (gFlipValue == -1.0f) {

		vsOut.Tex[0] = 1.0f - vsOut.Tex[0];
	}

	vsOut.Tex[1] = vertex.Tex[1];


	return vsOut;



}



float4 PS_Main(VertexShaderOut frag) : SV_TARGET{

	//Get the texel and convert to pixl display
	float4 color = gSpriteTex.Sample(samPoint,frag.Tex);

	//The alpha value for the texture;
	float alphaValue = color.w;


	color = lerp(color, gAltColor, gLerpValue);

	color.w = alphaValue;

	color.w = min(color.w, gOpacityValue);

	return color;
}



technique11 Sprite {

	pass P0 {

		SetVertexShader(CompileShader(vs_5_0, VS_Sprite()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS_Main()));


	}




};
